package com.artjoker.sample;


import android.app.Application;
import android.graphics.Typeface;

import com.crashlytics.android.Crashlytics;
import com.artjoker.sample.db.DatabaseInitializer;
import com.artjoker.sample.model.user.User;

import io.fabric.sdk.android.Fabric;
import com.artjoker.sample.di.AppComponent;
import com.artjoker.sample.di.DaggerAppComponent;
import com.artjoker.sample.di.modules.ContextModule;
import com.artjoker.sample.di.modules.DatabaseModule;
import com.artjoker.sample.di.modules.NetworkErrorModule;
import com.artjoker.sample.di.modules.UserModule;
import com.artjoker.sample.net.DefaultErrorHandler;
import com.artjoker.sample.utils.SystemHelper;

public class BaseApplication extends Application {

    public static AppComponent getAppComponent() {
        return appComponent;
    }

    private static AppComponent appComponent;

    private Typeface novaFont;

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());
        SystemHelper.getInstance().setContext(this);
        appComponent = DaggerAppComponent.builder()
                .contextModule(new ContextModule(this))
                .userModule(new UserModule(new User()))
                .databaseModule(new DatabaseModule(this, new DatabaseInitializer()))
                .networkErrorModule(new NetworkErrorModule(new DefaultErrorHandler(this)))
                .build();

        novaFont = Typeface.createFromAsset(getAssets(), "proximanovabold.otf");
    }

    public Typeface getNovaFont() {
        return novaFont;
    }
}
