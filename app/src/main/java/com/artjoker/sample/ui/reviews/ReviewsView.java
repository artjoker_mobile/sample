package artjoker.sample.ui.reviews;

import com.artjoker.sample.model.shop.ReviewModel;
import com.artjoker.sample.model.shop.ShopModel;

import java.util.List;

import artjoker.sample.BaseView;

public interface ReviewsView extends BaseView {

    void init(ShopModel shop);

    void addReviewsInAdapter(List<ReviewModel> reviews);

    void goToAuthorization();

    void goToWebShop(ShopModel model);
}
