package com.artjoker.sample.ui.account.promoCodes;

import com.artjoker.sample.model.PromoCodeModel;

import java.util.List;

import com.artjoker.sample.BaseView;

public interface PromoCodesView extends BaseView {

    void initPromoCodes(List<PromoCodeModel> promoCodes);

    void showApplyPromoCodeProgress();

    void hideApplyPromoCodeProgress();

    void handleError();
}
