package smarty.artjoker.sample.ui.favorites;

import android.support.v4.app.Fragment;

import smarty.artjoker.sample.NavigationActivity;
import smarty.artjoker.sample.R;

public class FavoritesActivity extends NavigationActivity {

    @Override
    protected int getNavigationMenuButtonId() {
        return R.id.favorites;
    }

    @Override
    protected Fragment startFragment() {
        return FavoritesFragment.newInstance();
    }

    @Override
    public boolean needBottomNavigation() {
        return true;
    }

    @Override
    public boolean needAnimation() {
        return false;
    }
}
