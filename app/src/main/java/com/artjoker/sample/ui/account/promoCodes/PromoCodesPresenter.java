package com.artjoker.sample.ui.account.promoCodes;

import com.arellomobile.mvp.InjectViewState;

import javax.inject.Inject;

import com.artjoker.sample.BaseApplication;
import com.artjoker.sample.BasePresenter;
import com.artjoker.sample.interactors.PromoCodesInteractor;
import com.artjoker.sample.interactors.ShopInteractor;
import com.artjoker.sample.net.DefaultErrorHandler;
import com.artjoker.sample.utils.rx.RxTransformers;

@InjectViewState
public class PromoCodesPresenter extends BasePresenter<PromoCodesView> {

    @Inject
    DefaultErrorHandler errorHandler;
    @Inject
    PromoCodesInteractor promoCodesInteractor;
    @Inject
    ShopInteractor shopInteractor;

    public PromoCodesPresenter() {
        BaseApplication.getAppComponent().inject(this);
    }

    public void loadPromoCodes(boolean refresh) {
        getDisposables().add(promoCodesInteractor.loadPromoCodes(refresh, promoCodeModels -> getViewState().initPromoCodes(promoCodeModels))
                .doOnError(throwable -> getViewState().handleError())
                .compose(RxTransformers.applyErrorHandling(errorHandler))
                .compose(RxTransformers.applyProgress(showProgress(), hideProgress()))
                .subscribe()
        );
    }

    public void applyPromoCode(String code) {
        getDisposables().add(promoCodesInteractor.applyPromoCode(code)
                .compose(RxTransformers.applySchedulers())
                .compose(RxTransformers.applyProgress(showApplyPromoCodeProgress(), hideApplyPromoCodeProgress()))
                .subscribe(promoCodeModels -> getViewState().initPromoCodes(promoCodeModels))
        );
    }

    public Runnable showApplyPromoCodeProgress() {
        return new Runnable() {
            @Override
            public void run() {
                getViewState().showApplyPromoCodeProgress();
            }
        };
    }

    public Runnable hideApplyPromoCodeProgress() {
        return new Runnable() {
            @Override
            public void run() {
                getViewState().hideApplyPromoCodeProgress();
            }
        };
    }

}
