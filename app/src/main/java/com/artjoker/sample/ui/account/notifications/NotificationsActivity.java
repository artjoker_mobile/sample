package com.artjoker.sample.ui.account.notifications;

import android.support.v4.app.Fragment;

import com.artjoker.sample.navigation.NavigationFloatingActivity;

public class NotificationsActivity extends NavigationFloatingActivity {

    @Override
    protected Fragment startFragment() {
        return NotificationsFragment.newInstance();
    }
}
