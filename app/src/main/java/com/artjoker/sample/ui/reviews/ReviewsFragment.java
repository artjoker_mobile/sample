package artjoker.sample.ui.reviews;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.artjoker.sample.model.shop.ReviewModel;
import com.artjoker.sample.model.shop.ShopDetailsModel;
import com.artjoker.sample.model.shop.ShopModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import artjoker.sample.MvpBaseFragment;
import artjoker.sample.R;
import artjoker.sample.dialogs.addReview.AddReviewDialog;
import artjoker.sample.ui.authorization.AuthorizationActivity;
import artjoker.sample.ui.web.WebShopActivity;
import artjoker.sample.utils.Utils;

import static artjoker.sample.R.string.reviews;

public class ReviewsFragment extends MvpBaseFragment implements ReviewsView {

    private static final String SHOP_KEY = "shop";
    private static final String SHOP_DETAILS_KEY = "shopDetails";

    @InjectPresenter(type = PresenterType.LOCAL)
    ReviewsPresenter reviewsPresenter;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.reviewsContainer)
    RecyclerView reviewsContainer;

    private ActionBar actionBar;
    private ReviewsAdapter reviewsAdapter;
    private ShopModel shop;

    @Override
    protected int getProgressID() {
        return 0;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_reviews;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ReviewsActivity reviewActivity = ((ReviewsActivity) getActivity());
        reviewActivity.setSupportActionBar(toolbar);
        actionBar = reviewActivity.getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setDisplayShowHomeEnabled(true);
        }
        toolbar.setPadding(0, Utils.getStatusBarHeight(getActivity()), 0, 0);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getActivity().finish();
            }
        });
        actionBar.setTitle(reviews);

        LinearLayoutManager reviewsLayoutManager = new LinearLayoutManager(getActivity());
        reviewsContainer.setLayoutManager(reviewsLayoutManager);
        reviewsContainer.setHasFixedSize(true);

        ShopDetailsModel shopDetailModel = (ShopDetailsModel) getArguments().getSerializable(SHOP_DETAILS_KEY);

        reviewsAdapter = new ReviewsAdapter(getContext(), shopDetailModel, new ArrayList<ReviewModel>(), true);

        reviewsAdapter.getAddReviewAction().subscribe(aBoolean -> {
            AddReviewDialog dialog = AddReviewDialog.newInstance(shopDetailModel.getId());
            dialog.show(getFragmentManager(), AddReviewDialog.TAG);
        });

        reviewsContainer.setAdapter(reviewsAdapter);

        reviewsPresenter.init((ShopModel) getArguments().getSerializable(SHOP_KEY));

        reviewsContainer.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (reviewsPresenter.isLoading() || reviewsAdapter.getReviews().size() == shop.getReviewsCount()) {
                    return;
                }
                int visibleItemCount = reviewsLayoutManager.getChildCount();
                int totalItemCount = reviewsLayoutManager.getItemCount();
                int pastVisibleItems = reviewsLayoutManager.findFirstVisibleItemPosition();
                if (pastVisibleItems + visibleItemCount >= totalItemCount) {
                    loadNextPage();
                }
            }
        });

        loadNextPage();

        return v;
    }

    public static ReviewsFragment newInstance(ShopModel shop, ShopDetailsModel shopDetailsModel) {
        Bundle args = new Bundle();
        args.putSerializable(SHOP_KEY, shop);
        args.putSerializable(SHOP_DETAILS_KEY, shopDetailsModel);
        ReviewsFragment fragment = new ReviewsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @OnClick(R.id.goToShop)
    public void onGoToShop() {
        reviewsPresenter.goToShop();
    }

    @Override
    public void init(ShopModel shop) {
        this.shop = shop;
        initTitle();
    }

    private void initTitle() {
        actionBar.setTitle(getString(R.string.reviewsCount, shop.getReviewsCount()));
    }

    @Override
    public void showProgress() {
    }

    @Override
    public void hideProgress() {

    }

    @Override
    public void goToAuthorization() {
        startActivity(AuthorizationActivity.getIntent(getActivity()));
    }

    @Override
    public void goToWebShop(ShopModel model) {
        startActivity(WebShopActivity.newIntent(getActivity(), model));
    }

    private void loadNextPage() {
        reviewsAdapter.addLoading();
        reviewsPresenter.loadNextPage();
    }

    @Override
    public void addReviewsInAdapter(List<ReviewModel> reviews) {
        if (reviewsAdapter.lastItemIsLoading()) {
            reviewsAdapter.removeLoading();
        }
        reviewsAdapter.addReviews(reviews);
    }

    public void onAddReview(ReviewModel model) {
        shop.setReviewsCount(shop.getReviewsCount() + 1);
        initTitle();
        reviewsAdapter.addCustomReview(model);
    }
}
