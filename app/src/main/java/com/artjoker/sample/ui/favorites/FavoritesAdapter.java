package smarty.artjoker.sample.ui.favorites;

import android.app.Activity;
import android.graphics.Paint;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.jakewharton.rxbinding2.view.RxView;
import com.artjoker.sample.smartysale.model.shop.ShopModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import smarty.artjoker.sample.BindViewHolder;
import smarty.artjoker.sample.R;
import smarty.artjoker.sample.utils.ShopUtils;
import smarty.artjoker.sample.widgets.AutofitTextView;
import smarty.artjoker.sample.widgets.CircleProgressBar;
import smarty.artjoker.sample.widgets.SwipeLayout;

class FavoritesAdapter extends RecyclerView.Adapter<FavoritesAdapter.FavoritesViewHolder> {

    private List<ShopModel> shops = new ArrayList<>();

    private Activity activity;
    private CallBacks callBacks;

    FavoritesAdapter(Activity activity, CallBacks callBacks) {
        this.activity = activity;
        this.callBacks = callBacks;
    }

    @Override
    public FavoritesViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.widget_item_favorites, parent, false);
        return new FavoritesViewHolder(view);
    }

    @Override
    public void onBindViewHolder(FavoritesViewHolder holder, int position) {
        holder.swipeLayout.reset();
        ShopModel shop = shops.get(position);
        holder.shopNameTextView.setText(shop.getTitle());
        ShopUtils.initCashBack(shop, holder.noActionCashBack, holder.cashBack);
        ShopUtils.loadShopLogo(activity, holder.shopLogo, shop, holder.logoProgressBar);

        RxView.clicks(holder.inShopLayout)
                .subscribe(o -> callBacks.onInShopAction(shop));
        RxView.clicks(holder.deleteLinearLayout)
                .subscribe(o -> callBacks.onUnFavorite(shop));

        initSwipe(holder.inShopLayout, holder.swipeLayout, holder.inShopProgressBar, shop);
    }

    @Override
    public int getItemCount() {
        int size = shops.size();
        callBacks.onSizeChange(size);
        return size;
    }

    void reset(List<ShopModel> shops) {
        this.shops.clear();
        this.shops.addAll(shops);
        notifyDataSetChanged();
    }

    public void remove(ShopModel shop) {
        for (int i = 0; i < shops.size(); i++) {
            if (shops.get(i).getId() == shop.getId()) {
                shops.remove(i);
                notifyItemRemoved(i);
                return;
            }
        }
    }

    static class FavoritesViewHolder extends BindViewHolder {

        @BindView(R.id.inShopProgressBar)
        CircleProgressBar inShopProgressBar;
        @BindView(R.id.in_shop_layout)
        LinearLayout inShopLayout;
        @BindView(R.id.delete_LinearLayout)
        LinearLayout deleteLinearLayout;
        @BindView(R.id.shopLogo)
        ImageView shopLogo;
        @BindView(R.id.logoProgressBar)
        ProgressBar logoProgressBar;
        @BindView(R.id.shopName_TextView)
        TextView shopNameTextView;
        @BindView(R.id.noActionCashBack)
        TextView noActionCashBack;
        @BindView(R.id.cashBack)
        AutofitTextView cashBack;
        @BindView(R.id.swipeLayout)
        SwipeLayout swipeLayout;
        @BindView(R.id.favorite_CardView)
        CardView card;

        public FavoritesViewHolder(View itemView) {
            super(itemView);
            noActionCashBack.setPaintFlags(noActionCashBack.getPaintFlags() | Paint.STRIKE_THRU_TEXT_FLAG);
        }
    }

    private void initSwipe(LinearLayout inShopLayout, SwipeLayout swipeLayout, CircleProgressBar circleProgressBar, ShopModel shop) {
        swipeLayout.setOnSwipeListener(new SwipeLayout.OnSwipeListener() {
            @Override
            public void onBeginSwipe(SwipeLayout swipeLayout, boolean moveToRight) {
            }

            @Override
            public void onSwipeToAction(SwipeLayout swipeLayout, float swipePercent, boolean moveToRight) {
                if(moveToRight) {
                    inShopLayout.setTranslationX(100 * swipePercent);
                    circleProgressBar.setProgress((int)(100 * swipePercent), false);
                }
            }

            @Override
            public void onCloseAnimation(SwipeLayout swipeLayout, float closePercent, boolean moveToRight) {
                if(moveToRight) {
                    inShopLayout.setTranslationX(100 * closePercent);
                }
            }

            @Override
            public void onActionSwiped(SwipeLayout swipeLayout, boolean moveToRight) {
                if(moveToRight) {
                    callBacks.onInShopAction(shop);
                    circleProgressBar.setProgress(0, false);
                }
            }

            @Override
            public void onSwipeClicked(SwipeLayout swipeLayout) {
                callBacks.onCardClicked(shop);
            }
        });
    }

    public interface CallBacks {
        void onSizeChange(int size);
        void onInShopAction(ShopModel shop);
        void onUnFavorite(ShopModel shop);
        void onCardClicked(ShopModel shop);
    }
}
