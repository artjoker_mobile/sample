package smarty.artjoker.sample.ui.reviews;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.Fragment;

import com.artjoker.sample.smartysale.model.shop.ReviewModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;
import com.artjoker.sample.smartysale.model.shop.ShopModel;

import smarty.artjoker.sample.MvpBaseActivity;
import smarty.artjoker.sample.R;
import smarty.artjoker.sample.dialogs.addReview.AddReviewDialog;

public class ReviewsActivity extends MvpBaseActivity implements AddReviewDialog.AddReviewListener {

    private static final String SHOP_KEY = "shop";
    private static final String SHOP_DETAILS_KEY = "shopDetails";

    @Override
    protected int getLayoutId() {
        return R.layout.activity_base;
    }

    @Override
    protected int getContainerId() {
        return R.id.container;
    }

    @Override
    protected Fragment startFragment() {
        return ReviewsFragment.newInstance((ShopModel) getIntent().getSerializableExtra(SHOP_KEY), (ShopDetailsModel) getIntent().getSerializableExtra(SHOP_DETAILS_KEY));
    }

    public static Intent getIntent(Context context, ShopModel shop, ShopDetailsModel shopDetails) {
        Intent intent = new Intent(context, ReviewsActivity.class);
        intent.putExtra(SHOP_KEY, shop);
        intent.putExtra(SHOP_DETAILS_KEY, shopDetails);
        return intent;
    }

    @Override
    public void onAddReview(ReviewModel model) {
        ((ReviewsFragment) getSupportFragmentManager().findFragmentById(R.id.container)).onAddReview(model);
    }
}
