package smarty.artjoker.sample.ui.favorites;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.jakewharton.rxbinding2.view.RxView;
import com.artjoker.sample.smartysale.model.shop.ShopModel;

import java.util.List;

import butterknife.BindView;
import smarty.artjoker.sample.MvpBaseFragment;
import smarty.artjoker.sample.NavigationActivity;
import smarty.artjoker.sample.R;
import smarty.artjoker.sample.ui.account.orders.OrdersActivity;
import smarty.artjoker.sample.ui.shopDetail.ShopDetailActivity;
import smarty.artjoker.sample.ui.web.WebShopActivity;
import smarty.artjoker.sample.utils.Utils;

public class FavoritesFragment extends MvpBaseFragment implements FavoritesView {

    @InjectPresenter(type = PresenterType.LOCAL)
    FavoritesPresenter presenter;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.favorites_Toolbar)
    Toolbar toolbar;
    @BindView(R.id.favorites_RecyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.toShopsCatalog_Button)
    Button toShopsButton;
    @BindView(R.id.watchMyOrders_TextView)
    TextView watchMyOrders;
    @BindView(R.id.noFavorites_LinearLayout)
    LinearLayout noFavoritesLinearLayout;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;

    FavoritesAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = super.onCreateView(inflater, container, savedInstanceState);

        toolbar.setPadding(0, Utils.getStatusBarHeight(getActivity()), 0, 0);
        
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        adapter = new FavoritesAdapter(getActivity(), new FavoritesAdapter.CallBacks() {
            @Override
            public void onSizeChange(int size) {
                toolbar.setTitle(Utils.getTitle(getActivity(), R.string.favorites_with_count, size));
            }

            @Override
            public void onInShopAction(ShopModel shop) {
                startActivity(WebShopActivity.newIntent(getActivity(), shop));
            }

            @Override
            public void onUnFavorite(ShopModel shop) {
                presenter.unFavorite(shop);
            }

            @Override
            public void onCardClicked(ShopModel shop) {
                startActivity(ShopDetailActivity.newIntent(getActivity(), shop));
            }
        });

        recyclerView.setAdapter(adapter);

        RxView.clicks(toShopsButton)
                .subscribe(o -> NavigationActivity.startShopsActivity(getActivity()));
        RxView.clicks(watchMyOrders)
                .subscribe(o -> startActivity(new Intent(getActivity(), OrdersActivity.class)));

        DrawerLayout drawerLayout = ((FavoritesActivity) getActivity()).getDrawerLayout();
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.menu_open, R.string.menu_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        swipeRefreshLayout.setColorSchemeResources(R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.resetState(true));

        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.resetState(false);
    }

    @Override
    protected int getProgressID() {
        return 0;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_favorites;
    }

    public static FavoritesFragment newInstance() {
        return new FavoritesFragment();
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void resetState(List<ShopModel> shops) {
        adapter.reset(shops);
        invalidate(shops.size());
    }

    @Override
    public void removeShop(ShopModel shop) {
        adapter.remove(shop);
        invalidate(adapter.getItemCount());
    }

    private void invalidate(int shopsSize) {
        boolean isEmpty = shopsSize == 0;
        noFavoritesLinearLayout.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
        recyclerView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
    }
}
