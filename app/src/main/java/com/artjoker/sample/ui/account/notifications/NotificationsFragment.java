package com.artjoker.sample.ui.account.notifications;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.SwitchCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.artjoker.sample.model.Notify;
import com.artjoker.sample.model.Notify.Notifications;

import butterknife.BindView;
import com.artjoker.sample.MvpBaseFragment;
import com.artjoker.sample.R;
import com.artjoker.sample.utils.Utils;

public class NotificationsFragment extends MvpBaseFragment implements NotificationsView {

    @InjectPresenter
    NotificationsPresenter presenter;

    @BindView(R.id.notifications_toolbar)
    Toolbar toolbar;

    @BindView(R.id.promotions_SwitchCompat)
    SwitchCompat promotionsSwitchCompat;
    @BindView(R.id.coupons_SwitchCompat)
    SwitchCompat couponsSwitchCompat;
    @BindView(R.id.newShops_SwitchCompat)
    SwitchCompat newShopsSwitchCompat;
    @BindView(R.id.newsService_SwitchCompat)
    SwitchCompat newsServiceSwitchCompat;
    @BindView(R.id.shopNews_SwitchCompat)
    SwitchCompat shopNewsSwitchCompat;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = super.onCreateView(inflater, container, savedInstanceState);

        presenter.initState();
        toolbar.setPadding(0, Utils.getStatusBarHeight(getActivity()), 0, 0);

        DrawerLayout drawerLayout = ((NotificationsActivity) getActivity()).getDrawerLayout();
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.menu_open, R.string.menu_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
        return view;
    }

    public static NotificationsFragment newInstance() {
        return new NotificationsFragment();
    }

    @Override
    protected int getProgressID() {
        return 0;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_notifications;
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.INVISIBLE);
    }

    @Override
    public void initNotify(Notify notify) {
        promotionsSwitchCompat.setChecked(notify.getActions() != 0);
        couponsSwitchCompat.setChecked(notify.getCoupons() != 0);
        newShopsSwitchCompat.setChecked(notify.getNewshops() != 0);
        newsServiceSwitchCompat.setChecked(notify.getSitenews() != 0);
        shopNewsSwitchCompat.setChecked(notify.getShopsnews() != 0);

        promotionsSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) ->
                presenter.notifyChanged(Notifications.ACTIONS, isChecked));
        couponsSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) ->
                presenter.notifyChanged(Notifications.COUPONS, isChecked));
        newShopsSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) ->
                presenter.notifyChanged(Notifications.NEW_SHOPS, isChecked));
        newsServiceSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) ->
                presenter.notifyChanged(Notifications.SITE_NEWS, isChecked));
        shopNewsSwitchCompat.setOnCheckedChangeListener((buttonView, isChecked) ->
                presenter.notifyChanged(Notifications.SHOPS_NEWS, isChecked));
    }
}
