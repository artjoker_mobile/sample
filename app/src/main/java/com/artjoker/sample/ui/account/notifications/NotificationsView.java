package com.artjoker.sample.ui.account.notifications;

import com.artjoker.sample.model.Notify;

import com.artjoker.sample.BaseView;

public interface NotificationsView extends BaseView {

    void initNotify(Notify notify);
}
