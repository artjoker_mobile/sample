package artjoker.sample.ui.reviews;

import com.arellomobile.mvp.InjectViewState;
import com.artjoker.sample.SessionStorage;
import com.artjoker.sample.api.AppServerSpecs;
import com.artjoker.sample.model.shop.ShopModel;
import com.artjoker.sample.model.user.AuthType;
import com.artjoker.sample.preferences.PrefStorage;

import javax.inject.Inject;

import smarty.artjoker.sample.BaseApplication;
import smarty.artjoker.sample.BasePresenter;
import smarty.artjoker.sample.net.DefaultErrorHandler;
import smarty.artjoker.sample.utils.rx.RxTransformers;

@InjectViewState
public class ReviewsPresenter extends BasePresenter<ReviewsView> {

    @Inject
    AppServerSpecs api;
    @Inject
    DefaultErrorHandler errorHandler;
    @Inject
    PrefStorage storage;
    @Inject
    SessionStorage sessionStorage;

    private ShopModel shop;
    private int pageNumber = 1;
    private boolean isLoading;

    public ReviewsPresenter() {
        BaseApplication.getAppComponent().inject(this);
    }

    public void init(ShopModel shop) {
        this.shop = shop;
        getViewState().init(shop);
    }

    public void loadNextPage() {
        isLoading = true;
        getDisposables().add(api.getShopReviews(shop.getId(), pageNumber)
                .compose(RxTransformers.applySchedulers())
                .compose(RxTransformers.applyErrorHandling(errorHandler))
                .subscribe(newsModels -> {
                    getViewState().addReviewsInAdapter(newsModels);
                    pageNumber++;
                    isLoading = false;
                }, throwable -> isLoading = false)
        );
    }

    public void goToShop() {
        if (storage.loadAuthType() == AuthType.GUEST) {
            getViewState().goToAuthorization();
        } else {
            getViewState().goToWebShop(shop);
        }
    }

    public boolean isLoading() {
        return isLoading;
    }
}
