package com.artjoker.sample.ui.account.promoCodes;

import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.PresenterType;
import com.artjoker.sample.model.PromoCodeModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.functions.Consumer;
import com.artjoker.sample.MvpBaseFragment;
import com.artjoker.sample.R;
import com.artjoker.sample.utils.Ui;
import com.artjoker.sample.utils.Utils;
import com.artjoker.sample.utils.rx.RxBus;

public class PromoCodesFragment extends MvpBaseFragment implements PromoCodesView {

    @InjectPresenter(type = PresenterType.LOCAL)
    PromoCodesPresenter promoCodesPresenter;

    DrawerLayout drawerLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @BindView(R.id.applyPromoCode)
    Button applyPromoCode;
    @BindView(R.id.applyPromoCodeProgress)
    ProgressBar applyPromoCodeProgress;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.noPromoCodesContainer)
    LinearLayout noPromoCodesContainer;
    @BindView(R.id.promoCodeEditText)
    EditText promoCodeEditText;
    @BindView(R.id.promoCodesContainer)
    RecyclerView promoCodesContainer;

    private PromoCodesAdapter promoCodesAdapter;

    private RxBus<Boolean> changePromoCodeProgressVisibilityAction = new RxBus<>();

    @Override
    protected int getProgressID() {
        return 0;
    }

    @Override
    protected int getLayoutID() {
        return R.layout.fragment_promocodes;
    }

    public static PromoCodesFragment newInstance() {
        return new PromoCodesFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawerLayout = ((PromoCodesActivity) getActivity()).getDrawerLayout();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        toolbar.setPadding(0, Utils.getStatusBarHeight(getActivity()), 0, 0);

        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(getActivity(), drawerLayout, toolbar, R.string.menu_open, R.string.menu_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();

        promoCodeEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((event != null && (event.getKeyCode() == KeyEvent.KEYCODE_ENTER)) || (actionId == EditorInfo.IME_ACTION_DONE)) {
                    applyCode(promoCodeEditText.getText().toString());
                }
                return false;
            }
        });

        LinearLayoutManager promoCodesLayoutManager = new LinearLayoutManager(getActivity());
        promoCodesContainer.setLayoutManager(promoCodesLayoutManager);
        promoCodesAdapter = new PromoCodesAdapter(getActivity(), new ArrayList<>(), changePromoCodeProgressVisibilityAction.toObservable());
        promoCodesContainer.setAdapter(promoCodesAdapter);

        promoCodesPresenter.loadPromoCodes(false);

        promoCodesAdapter.getApplyPromoCodeAction().subscribe(new Consumer<String>() {
            @Override
            public void accept(String code) throws Exception {
                applyCode(code);
            }
        });

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                promoCodesPresenter.loadPromoCodes(true);
            }
        });

        return v;
    }

    @OnClick(R.id.applyPromoCode)
    public void onApplyPromoCodeClick() {
        applyCode(promoCodeEditText.getText().toString());
    }

    @Override
    public void showProgress() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.setVisibility(View.GONE);
    }
    @Override
    public void initPromoCodes(List<PromoCodeModel> promoCodesList) {
        swipeRefreshLayout.setRefreshing(false);
        if(!promoCodesList.isEmpty()) {
            promoCodesContainer.setVisibility(View.VISIBLE);
            noPromoCodesContainer.setVisibility(View.GONE);
            promoCodesAdapter.setPromoCodes(promoCodesList);
            promoCodesAdapter.notifyDataSetChanged();
        } else {
            noPromoCodesContainer.setVisibility(View.VISIBLE);
            promoCodesContainer.setVisibility(View.GONE);
        }
    }

    @Override
    public void showApplyPromoCodeProgress() {
        applyPromoCode.setEnabled(false);
        applyPromoCodeProgress.setVisibility(View.VISIBLE);
        changePromoCodeProgressVisibilityAction.send(true);
    }

    @Override
    public void hideApplyPromoCodeProgress() {
        applyPromoCodeProgress.setVisibility(View.GONE);
        applyPromoCode.setEnabled(true);
        changePromoCodeProgressVisibilityAction.send(false);
    }

    private void applyCode(String code) {
        Ui.hideKeyboard(getActivity(), getView());
        promoCodesPresenter.applyPromoCode(code);
    }

    @Override
    public void handleError() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
