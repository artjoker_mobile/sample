package com.artjoker.sample.ui.account.promoCodes;

import android.support.v4.app.Fragment;

import com.artjoker.sample.navigation.NavigationFloatingActivity;

public class PromoCodesActivity extends NavigationFloatingActivity {

    @Override
    protected Fragment startFragment() {
        return PromoCodesFragment.newInstance();
    }
}
