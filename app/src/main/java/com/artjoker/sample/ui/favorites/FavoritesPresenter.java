package artjoker.sample.ui.favorites;

import com.arellomobile.mvp.InjectViewState;
import com.artjoker.sample.api.models.responses.ShopsResponse;
import com.artjoker.sample.model.shop.ShopModel;

import javax.inject.Inject;

import io.reactivex.Observable;
import artjoker.sample.BaseApplication;
import artjoker.sample.BasePresenter;
import artjoker.sample.interactors.ShopInteractor;
import artjoker.sample.net.DefaultErrorHandler;
import artjoker.sample.utils.ShopUtils;
import artjoker.sample.utils.rx.RxTransformers;

@InjectViewState
public class FavoritesPresenter extends BasePresenter<FavoritesView> {

    @Inject
    ShopInteractor shopInteractor;
    @Inject
    DefaultErrorHandler errorHandler;

    public FavoritesPresenter() {
        BaseApplication.getAppComponent().inject(this);
    }

    public void resetState(boolean isForce) {
        Observable<ShopsResponse> shopsResponseObservable;
        if (isForce) {
            shopsResponseObservable = shopInteractor.loadApiShops()
                    .compose(RxTransformers.applyErrorHandling(errorHandler));
        } else {
            shopsResponseObservable = shopInteractor.loadDbShops();
        }
        shopsResponseObservable
                .compose(RxTransformers.applySchedulers())
                .compose(RxTransformers.applyProgress(showProgress(), hideProgress()))
                .map(ShopUtils::getOnShops)
                .map(shopsResponse -> ShopUtils.getFavoritesShops(shopsResponse.getShops()))
                .subscribe(getViewState()::resetState);
    }

    public void unFavorite(ShopModel shop) {
        getViewState().removeShop(shop);
        shopInteractor.deleteShopFromFavorites(shop)
                .compose(RxTransformers.applySchedulers())
                .compose(RxTransformers.applyErrorHandling(errorHandler))
                .compose(RxTransformers.applyProgress(showProgress(), hideProgress()))
                .subscribe();
    }
}
