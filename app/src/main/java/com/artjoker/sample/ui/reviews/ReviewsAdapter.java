package smarty.artjoker.sample.ui.reviews;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.artjoker.sample.smartysale.model.shop.ReviewModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import smarty.artjoker.sample.BindViewHolder;
import smarty.artjoker.sample.R;
import smarty.artjoker.sample.utils.Utils;
import smarty.artjoker.sample.utils.rx.RxBus;
import smarty.artjoker.sample.widgets.ProgressViewHolder;

import static smarty.artjoker.sample.utils.ShopUtils.REVIEWS_AVATAR_DIRECTORY;

public class ReviewsAdapter extends RecyclerView.Adapter {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;
    private static final int TYPE_PROG = 2;

    private List<ReviewModel> reviews;
    private Context context;

    private RxBus<Boolean> addReviewAction = new RxBus<>();

    private ShopDetailsModel shopDetails;

    private boolean needLoadingProgress;

    public ReviewsAdapter(Context context, ShopDetailsModel shopDetails, ArrayList<ReviewModel> reviews, boolean needLoadingProgress) {
        this.context = context;
        this.shopDetails = shopDetails;
        this.reviews = reviews;
        this.needLoadingProgress = needLoadingProgress;
    }

    public List<ReviewModel> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewModel> reviews) {
        this.reviews = reviews;
    }

    public Observable<Boolean> getAddReviewAction() {
        return addReviewAction.toObservable();
    }

    public ShopDetailsModel getShopDetails() {
        return shopDetails;
    }

    public void setShopDetails(ShopDetailsModel shopDetails) {
        this.shopDetails = shopDetails;
    }

    public void addCustomReview(ReviewModel reviewModel) {
        reviews.add(0, reviewModel);
        notifyItemInserted(1);
    }

    public void addReviews(List<ReviewModel> newReviewsList) {
        int startPosition = reviews.size() + 1;
        reviews.addAll(newReviewsList);
        notifyItemRangeInserted(startPosition, newReviewsList.size());
    }

    public boolean lastItemIsLoading() {
        return reviews.get(reviews.size() - 1) == null;
    }

    public void addLoading() {
        reviews.add(null);
        notifyItemInserted(reviews.size());
    }

    public void removeLoading() {
        reviews.remove(reviews.size() - 1);
        notifyItemRemoved(reviews.size() + 1);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return new HeaderItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_add_review_info, parent, false));
        } else if (viewType == TYPE_PROG) {
            return new ProgressViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.progress_small, parent, false));
        } else {
            return new ReviewItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_item_review, parent, false));
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return !needLoadingProgress || reviews.get(position - 1) != null ? TYPE_ITEM : TYPE_PROG;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ReviewItemHolder) {
            ReviewItemHolder itemHolder = (ReviewItemHolder) holder;
            ReviewModel review = reviews.get(position - 1);
            itemHolder.init(review, position);
        } else if (holder instanceof HeaderItemHolder) {
            HeaderItemHolder itemHolder = (HeaderItemHolder) holder;
            itemHolder.init();
        }
    }

    @Override
    public int getItemCount() {
        return reviews.size() + 1;
    }

    class HeaderItemHolder extends BindViewHolder {
        @BindView(R.id.goToShopLayout)
        LinearLayout goToShopLayout;
        @BindView(R.id.addReview)
        Button addReview;

        HeaderItemHolder(View itemView) {
            super(itemView);
        }

        public void init() {
            if (shopDetails.getMyOrders() > 0) {
                goToShopLayout.setVisibility(View.GONE);
                addReview.setVisibility(View.VISIBLE);
            } else {
                goToShopLayout.setVisibility(View.VISIBLE);
                addReview.setVisibility(View.GONE);
            }
        }

        @OnClick(R.id.addReview)
        public void onAddReviewClick() {
            addReviewAction.send(true);
        }
    }

    public class ReviewItemHolder extends BindViewHolder {

        @BindView(R.id.reviewItem)
        LinearLayout reviewItem;
        @BindView(R.id.author)
        TextView author;
        @BindView(R.id.date)
        TextView date;
        @BindView(R.id.avatar)
        ImageView avatar;
        @BindView(R.id.rating)
        RatingBar rating;
        @BindView(R.id.text)
        TextView text;
        @BindView(R.id.answer)
        TextView answer;

        ReviewItemHolder(View itemView) {
            super(itemView);
        }

        public void init(final ReviewModel model, int position) {
            author.setText(model.getAuthor());
            date.setText(Utils.formatDate(model.getDate()));
            rating.setRating(model.getRating());
            text.setText(model.getText());

            if (position < getItemCount() - 1) {
                reviewItem.setBackgroundResource(R.drawable.bottom_line_1dp);
            }

            if (!Utils.isEmpty(model.getAnswer())) {
                answer.setText(model.getAnswer());
                answer.setVisibility(View.VISIBLE);
            } else {
                answer.setVisibility(View.GONE);
            }

            Utils.loadRemoteAvatar(context, avatar, REVIEWS_AVATAR_DIRECTORY + model.getAvatar());
        }
    }
}
