package .rtjoker.sample.ui.favorites;

import com.artjoker.sample.model.shop.ShopModel;

import java.util.List;

import artjoker.sample.BaseView;

public interface FavoritesView extends BaseView {
    void resetState(List<ShopModel> shops);
    void removeShop(ShopModel shop);
}
