package com.artjoker.sample.ui.account.notifications;

import com.arellomobile.mvp.InjectViewState;
import com.artjoker.sample.model.Notify;
import com.artjoker.sample.model.Notify.Notifications;
import com.artjoker.sample.preferences.PrefStorage;

import javax.inject.Inject;

import com.artjoker.sample.BaseApplication;
import com.artjoker.sample.BasePresenter;
import com.artjoker.sample.ui.account.notifications.domain.NotificationsInteractor;
import com.artjoker.sample.utils.rx.RxCompletableTransformers;
import com.artjoker.sample.utils.rx.RxTransformers;

@InjectViewState
public class NotificationsPresenter extends BasePresenter<NotificationsView> {

    @Inject
    NotificationsInteractor interactor;
    @Inject
    PrefStorage storage;

    public NotificationsPresenter() {
        BaseApplication.getAppComponent().inject(this);
    }

    public void initState() {
        interactor.getNotify()
                .compose(RxTransformers.applyProgress(showProgress(), hideProgress()))
                .subscribe(getViewState()::initNotify);
    }

    public void notifyChanged(Notifications notifications, Boolean isActive) {
        int value = isActive ? 1 : 0;
        interactor.changeNotify(notifications, value)
                .compose(RxCompletableTransformers.applyProgress(showProgress(), hideProgress()))
                .subscribe(() -> {
                    Notify storageNotify = storage.getNotify();
                    switch (notifications) {
                        case ACTIONS:
                            storageNotify.setActions(value);
                            break;
                        case NEW_SHOPS:
                            storageNotify.setNewshops(value);
                            break;
                        case COUPONS:
                            storageNotify.setCoupons(value);
                            break;
                        case SITE_NEWS:
                            storageNotify.setSitenews(value);
                            break;
                        case SHOPS_NEWS:
                            storageNotify.setShopsnews(value);
                            break;
                    }
                    storage.saveNotify(storageNotify);
                });
    }
}
