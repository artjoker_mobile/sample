package com.artjoker.sample.ui.account.promoCodes;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.artjoker.sample.smartysale.model.PromoCodeModel;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import io.reactivex.Observable;
import com.artjoker.sample.BaseApplication;
import com.artjoker.sample.BindViewHolder;
import com.artjoker.sample.R;
import com.artjoker.sample.utils.Utils;
import com.artjoker.sample.utils.rx.RxBus;

public class PromoCodesAdapter extends RecyclerView.Adapter {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ITEM = 1;

    private List<PromoCodeModel> promoCodes;
    private Activity activity;

    private RxBus<String> applyPromoCodeAction = new RxBus<>();

    private Observable<Boolean> changePromoCodeProgressVisibilityObservable;

    public PromoCodesAdapter(Activity activity, List<PromoCodeModel> promoCodes, Observable<Boolean> changePromoCodeProgressVisibilityObservable) {
        this.activity = activity;
        this.promoCodes = promoCodes;
        this.changePromoCodeProgressVisibilityObservable = changePromoCodeProgressVisibilityObservable;
    }

    public List<PromoCodeModel> getPromoCodes() {
        return promoCodes;
    }

    public void setPromoCodes(List<PromoCodeModel> promoCodes) {
        this.promoCodes = promoCodes;
    }

    public Observable<String> getApplyPromoCodeAction() {
        return applyPromoCodeAction.toObservable();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == TYPE_HEADER) {
            return new HeaderItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_promo_codes_header, parent, false));
        } else {
            return new PromoCodeItemHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.widget_item_promo_code, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof PromoCodeItemHolder) {
            PromoCodeItemHolder itemHolder = (PromoCodeItemHolder) holder;
            PromoCodeModel promoCode = promoCodes.get(position - 1);
            itemHolder.init(promoCode);
        }
    }

    @Override
    public int getItemCount() {
        return promoCodes.size() + 1;
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_HEADER;
        }
        return TYPE_ITEM;
    }

    class HeaderItemHolder extends BindViewHolder {

        @BindView(R.id.applyPromoCode)
        Button applyPromoCode;
        @BindView(R.id.applyPromoCodeProgress)
        ProgressBar applyPromoCodeProgress;
        @BindView(R.id.promoCodeEditText)
        TextView promoCodeEditText;

        HeaderItemHolder(View itemView) {
            super(itemView);

            changePromoCodeProgressVisibilityObservable.subscribe(progressVisibility -> {
                if(progressVisibility) {
                    applyPromoCode.setEnabled(false);
                    applyPromoCodeProgress.setVisibility(View.VISIBLE);
                } else {
                    applyPromoCodeProgress.setVisibility(View.GONE);
                    applyPromoCode.setEnabled(true);
                }
            });
        }

        @OnClick(R.id.applyPromoCode)
        public void onApplyPromoCodeClick() {
            applyPromoCodeAction.send(promoCodeEditText.getText().toString());
        }
    }

    public class PromoCodeItemHolder extends BindViewHolder {

        @BindView(R.id.promoCode)
        TextView promoCode;
        @BindView(R.id.title)
        TextView title;
        @BindView(R.id.applyDate)
        TextView applyDate;
        @BindView(R.id.endDate)
        TextView endDate;

        PromoCodeItemHolder(View itemView) {
            super(itemView);
            promoCode.setTypeface(((BaseApplication) activity.getApplication()).getNovaFont());
        }

        public void init(PromoCodeModel model) {
            promoCode.setText(model.getCode());
            title.setText(model.getTitle());
            applyDate.setText(activity.getString(R.string.applyDate, Utils.formatDateTime(model.getStartDate())));
            endDate.setText(activity.getString(R.string.endDate, Utils.formatDateTime(model.getEndDate())));
        }
    }
}
