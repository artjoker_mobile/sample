package com.artjoker.sample.net;

import android.content.Context;

import com.google.gson.Gson;


import java.net.UnknownHostException;
import java.util.Map;

import io.reactivex.Observable;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import com.artjoker.sample.R;
import com.artjoker.sample.dialogs.models.AlertModel;
import com.artjoker.sample.utils.rx.RxBus;

public class DefaultErrorHandler implements BIErrorHandler {

    private static final int UN_AUTHORIZED_CODE = 401;
    private Context context;
    private RxBus<String> unauthorizedBus = new RxBus<>();
    private RxBus<AlertModel> errorBus = new RxBus<>();

    public DefaultErrorHandler(Context context) {
        this.context = context;
    }

    @Override
    public void processNetError(Throwable throwable) {
        int message = 0;
        if (throwable instanceof HttpException) {
            HttpException httpException = (HttpException) throwable;
            if (httpException.code() == UN_AUTHORIZED_CODE) {
                unauthorizedBus.send(context.getString(R.string.un_authorized));
                return;
            }
            ResponseBody responseBody = httpException.response().errorBody();
            try {
                String string = responseBody.string();
                ErrorModel response = new Gson().fromJson(string, ErrorModel.class);
                showAlert(context.getString(R.string.error_happened), context.getString(R.string.try_support)+"\n"+ response.getName());
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (throwable instanceof UnknownHostException) {
            showAlert(context.getString(R.string.error), context.getString(R.string.connection_error));
        }
    }

    @Override
    public void showError(Map<String, String> errors) {
        Map.Entry<String, String> error = errors.entrySet().iterator().next();
        showAlert(context.getString(R.string.error), error.getValue());
    }

    private void showAlert(String title, String message) {
        errorBus.send(new AlertModel(title, message));
    }

    public RxBus<String> getUnauthorizedBus() {
        return unauthorizedBus;
    }

    public void setUnauthorizedBus(RxBus<String> unauthorizedBus) {
        this.unauthorizedBus = unauthorizedBus;
    }

    public Observable<AlertModel> getErrorBus() {
        return errorBus.toObservable();
    }

    public void setErrorBus(RxBus<AlertModel> errorBus) {
        this.errorBus = errorBus;
    }
}
