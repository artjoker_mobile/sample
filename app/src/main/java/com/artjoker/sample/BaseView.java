package com.artjoker.sample;

import com.arellomobile.mvp.MvpView;

/**
 * Created by dev on 11.01.17.
 */

public interface BaseView extends MvpView {
    void showProgress();
    void hideProgress();
}
