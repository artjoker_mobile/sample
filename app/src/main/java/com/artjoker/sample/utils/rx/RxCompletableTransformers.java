

import android.os.Handler;
import android.os.Looper;


import io.reactivex.Completable;
import io.reactivex.CompletableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class RxCompletableTransformers {

    public static CompletableTransformer applySchedulers() {
        return completable -> completable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static CompletableTransformer applyProgress(final Runnable before, final Runnable after) {
        return completable -> completable.doOnSubscribe(disposable -> new Handler(Looper.getMainLooper()).post(before))
                .doOnTerminate(() -> new Handler(Looper.getMainLooper()).post(after));
    }

    @SuppressWarnings("unchecked")
    public static CompletableTransformer applyErrorHandling(final BIErrorHandler errorHandler) {
        return completable -> completable
                .onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    errorHandler.processNetError(throwable);

                    return Completable.complete();
                });
    }
}
