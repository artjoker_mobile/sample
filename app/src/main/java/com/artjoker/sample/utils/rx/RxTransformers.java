package artjoker.sample.utils.rx;

import android.os.Handler;
import android.os.Looper;

import io.reactivex.Observable;
import io.reactivex.ObservableTransformer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Alex.Sinyaev on 05.04.16.
 */
public class RxTransformers {

    public static <T> ObservableTransformer<T, T> applySchedulers() {
        return tObservable -> tObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());
    }

    public static <T> ObservableTransformer<T, T> applyProgress(final Runnable before, final Runnable after) {
        return tObservable -> tObservable.doOnSubscribe(disposable -> new Handler(Looper.getMainLooper()).post(before))
                .doOnTerminate(() -> new Handler(Looper.getMainLooper()).post(after));
    }

    @SuppressWarnings("unchecked")
    public static <T> ObservableTransformer<T, T> applyErrorHandling(final BIErrorHandler errorHandler) {
        return tObservable -> tObservable
                .flatMap(t -> {
                    if (t instanceof Response) {
                        Response response = ((Response) t);
                        if (response.getResult() == 1) return Observable.just(t);
                        else {
                            if (response.getError() != null) {
                                errorHandler.showError(response.getError());
                            }
                            return Observable.empty();
                        }

                    } else return Observable.just(t);
                }).onErrorResumeNext(throwable -> {
                    throwable.printStackTrace();
                    errorHandler.processNetError(throwable);

                    return Observable.empty();
                });
    }
}
