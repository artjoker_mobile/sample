package com.artjoker.sample.receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.artjoker.sample.preferences.PrefStorage;

import javax.inject.Inject;

import com.artjoker.sample.BaseApplication;
import com.artjoker.sample.utils.Utils;

public class ReferrerReceiver extends BroadcastReceiver {
    private static final String KEY_REFERRER = "referrer";

    @Inject
    PrefStorage prefStorage;

    public ReferrerReceiver() {
        BaseApplication.getAppComponent().inject(this);
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();
        if (intent.getExtras() != null) {
            String referrer = extras.getString(KEY_REFERRER);
            if(!Utils.isEmpty(referrer)) {
                prefStorage.saveReferrer(referrer);
            }
        }
    }
}