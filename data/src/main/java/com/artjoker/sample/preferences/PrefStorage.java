package com.artjoker.sample.smartysale.preferences;

import com.artjoker.sample.smartysale.api.models.responses.Contacts;
import com.artjoker.sample.smartysale.model.Notify;
import com.artjoker.sample.smartysale.model.country.CountryModel;
import com.artjoker.sample.smartysale.model.user.AuthType;
import com.artjoker.sample.smartysale.model.user.User;

import java.util.List;

public interface PrefStorage {
    String loadSelectedCountry();
    void saveSelectedCountry(String country);

    List<CountryModel> loadCountries();
    void saveCountries(List<CountryModel> countries);

    boolean loadHowThisWorkBannerClosed();
    void saveHowThisWorkBannerClosed(boolean isClosed);

    boolean loadEarnWithUsBannerClosed();
    void saveEarnWithUsBannerClosed(boolean isClosed);

    boolean loadRateBannerClicked();
    void saveRateBannerClicked(boolean isClicked);

    long loadLastShopsUpdateTime();
    void saveLastShopsUpdateTime(long time);

    long loadLastNewsUpdateTime();
    void saveLastNewsUpdateTime(long time);

    long loadLastPromoCodesUpdateTime();
    void saveLastPromoCodesUpdateTime(long time);

    long loadLastMyReviewsUpdateTime();
    void saveLastMyReviewsUpdateTime(long time);

    long loadLastNotifyUpdateTime();

    long loadLastProfileUpdateTime();

    String getDetectedCountry();
    void saveDetectedCountry(String country);

    boolean isFirstAnimateShown();
    void setIsFirstAnimateShown(boolean isFirstAnimateShown);

    void saveUser(User user);
    User getUser();

    void saveNotify(Notify notify);
    Notify getNotify();

    void saveHeader(String header);
    String getHeader();

    void saveToken(String token);
    String getToken();

    int isConfirm();
    void setConfirm(int isConfirm);

    AuthType loadAuthType();
    void saveAuthType(AuthType authType);

    boolean getFirstLoadingIsCompleted();
    void setFirstLoadingIsCompleted(boolean value);

    void saveLastOrdersUpdateTime(long time);
    long loadLastOrdersUpdateTime();

    void savePushToken(String pushToken);
    String loadPushToken();

    void clearWithout(String... fields);

    void saveContacts(Contacts contacts);
    Contacts getContacts();

    boolean loadNeedChooseAppDialog();
    void saveNeedChooseAppDialog(boolean value);

    String loadReferrer();
    void saveReferrer(String referrer);

    void saveLastFaqUpdateTime(long time);
    long loadLastFaqUpdateTime();

    void saveLastRulesUpdateTime(long time);
    long loadLastRulesUpdateTime();

    void saveLaunchCount(int count);
    int loadLaunchCount();

    void saveLastSlidersUpdateTime(long time);
    long loadLastSlidersUpdateTime();
}
