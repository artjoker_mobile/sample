package com.artjoker.sample.smartysale.preferences;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.artjoker.sample.smartysale.api.models.responses.Contacts;
import com.artjoker.sample.smartysale.model.Notify;
import com.artjoker.sample.smartysale.model.country.CountryModel;
import com.artjoker.sample.smartysale.model.user.AuthType;
import com.artjoker.sample.smartysale.model.user.User;
import com.artjoker.sample.smartysale.utils.CryptoProxy;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.inject.Inject;

public class PrefStorageImpl implements PrefStorage {

    public static final String APP_PREFERENCES = "mysettings";

    public static final String SELECTED_COUNTRY_KEY = "selected_country_key";
    public static final String DETECTED_COUNTRY_KEY = "detected_country_key";
    public static final String COUNTRIES_KEY = "countries_key";
    public static final String CONTACTS_KEY = "contacts_key";
    public static final String HOW_THIS_WORK_CLOSED_KEY = "how_this_work_closed_key";
    public static final String EARN_WITH_US_CLOSED_KEY = "earn_with_us_closed_key";
    public static final String RATE_BANNER_CLICKED_KEY = "rate_banner_clicked_key";
    public static final String LAST_SHOPS_UPDATE_TIME_KEY = "last_shops_update_time_key";
    public static final String LAST_NEWS_UPDATE_TIME_KEY = "last_news_update_time_key";
    public static final String LAST_MY_REVIEWS_UPDATE_TIME_KEY = "last_my_reviews_update_time_key";
    public static final String LAST_NOTIFY_UPDATE_TIME_KEY = "last_notify_update_time_key";
    public static final String LAST_ORDERS_UPDATE_TIME_KEY = "last_orders_update_time_key";
    public static final String FIRST_LOADING_IS_COMPLETED_KEY = "first_loading_is_completed_key";
    public static final String LAST_FAQ_UPDATE_TIME_KEY = "last_faq_update_time_key";
    public static final String LAST_RULES_UPDATE_TIME_KEY = "last_rules_update_time_key";
    public static final String LAST_SLIDERS_UPDATE_TIME_KEY = "last_sliders_update_time_key";
    public static final String FIRST_ANIMATE_KEY = "first_animate_key";
    public static final String CRYPTO_USER_KEY = "crypto_user_key";
    public static final String NOTIFY_KEY = "notify_key";
    public static final String LAST_PROFILE_UPDATE_TIME_KEY = "last_profile_update_time_key";
    public static final String LAST_PROMO_CODE_UPDATE_TIME_KEY = "last_promo_code_update_time_key";
    public static final String REFERRER_KEY = "referrer_key";
    public static final String LAUNCH_COUNT_KEY = "launch_count_key";

    private static final String HEADER_KEY = "header_key";
    private static final String TOKEN_KEY = "TOKEN_key";
    private static final String CONFIRM_KEY = "confirm_key";
    public static final String AUTH_TYPE_KEY = "auth_type_key";

    public static final String PUSH_TOKEN_KEY = "push_token_key";
    public static final String NEED_DONT_ASK_ANYMORE_KEY = "need_dont_ask_anymore_key";

    private final CryptoProxy cryptoProxy;
    private final Preferences preferences;

    @Inject
    public PrefStorageImpl(Context context) {
        preferences = new Preferences(context, APP_PREFERENCES);
        cryptoProxy = new CryptoProxy(context);
    }

    @Override
    public String loadSelectedCountry() {
        return preferences.getString(SELECTED_COUNTRY_KEY);
    }

    @Override
    public void saveSelectedCountry(String country) {
        preferences.putString(SELECTED_COUNTRY_KEY, country);
    }

    @Override
    public List<CountryModel> loadCountries() {
        String countriesJson = preferences.getString(COUNTRIES_KEY);
        Type listType = new TypeToken<ArrayList<CountryModel>>(){}.getType();
        return new Gson().fromJson(countriesJson, listType);
    }

    @Override
    public void saveCountries(List<CountryModel> countries) {
        preferences.putString(COUNTRIES_KEY, new Gson().toJson(countries));
    }

    @Override
    public boolean loadHowThisWorkBannerClosed() {
        return preferences.getBoolean(HOW_THIS_WORK_CLOSED_KEY);
    }

    @Override
    public void saveHowThisWorkBannerClosed(boolean visibility) {
        preferences.putBoolean(HOW_THIS_WORK_CLOSED_KEY, visibility);
    }

    @Override
    public boolean loadEarnWithUsBannerClosed() {
        return preferences.getBoolean(EARN_WITH_US_CLOSED_KEY);
    }

    @Override
    public void saveEarnWithUsBannerClosed(boolean visibility) {
        preferences.putBoolean(EARN_WITH_US_CLOSED_KEY, visibility);
    }

    @Override
    public long loadLastShopsUpdateTime() {
        return preferences.getLong(LAST_SHOPS_UPDATE_TIME_KEY);
    }

    @Override
    public void saveLastShopsUpdateTime(long time) {
        preferences.putLong(LAST_SHOPS_UPDATE_TIME_KEY, time);
    }

    @Override
    public void saveLastMyReviewsUpdateTime(long time) {
        preferences.putLong(LAST_MY_REVIEWS_UPDATE_TIME_KEY, time);
    }

    @Override
    public long loadLastMyReviewsUpdateTime() {
        return preferences.getLong(LAST_MY_REVIEWS_UPDATE_TIME_KEY);
    }

    @Override
    public long loadLastNewsUpdateTime() {
        return preferences.getLong(LAST_NEWS_UPDATE_TIME_KEY);
    }

    @Override
    public void saveLastNewsUpdateTime(long time) {
        preferences.putLong(LAST_NEWS_UPDATE_TIME_KEY, time);
    }

    @Override
    public void saveRateBannerClicked(boolean isClicked) {
        preferences.putBoolean(RATE_BANNER_CLICKED_KEY, isClicked);
    }

    @Override
    public boolean loadRateBannerClicked() {
        return preferences.getBoolean(RATE_BANNER_CLICKED_KEY);
    }

    @Override
    public long loadLastNotifyUpdateTime() {
        return preferences.getLong(LAST_NOTIFY_UPDATE_TIME_KEY);
    }

    @Override
    public void saveLastPromoCodesUpdateTime(long time) {
        preferences.putLong(LAST_PROMO_CODE_UPDATE_TIME_KEY, time);
    }

    @Override
    public long loadLastPromoCodesUpdateTime() {
        return preferences.getLong(LAST_PROMO_CODE_UPDATE_TIME_KEY);
    }

    @Override
    public void setIsFirstAnimateShown(boolean isFirstAnimateShown) {
        preferences.putBoolean(FIRST_ANIMATE_KEY, isFirstAnimateShown);
    }

    @Override
    public boolean isFirstAnimateShown() {
        return preferences.getBoolean(FIRST_ANIMATE_KEY, false);
    }

    @Override
    public String getDetectedCountry() {
        return preferences.getString(DETECTED_COUNTRY_KEY);
    }

    @Override
    public void saveDetectedCountry(String country) {
        preferences.putString(DETECTED_COUNTRY_KEY, country);
    }

    @Override
    public void saveUser(User user) {
        if (user != null) {
            saveCrypto(user, CRYPTO_USER_KEY);
            preferences.putLong(LAST_PROFILE_UPDATE_TIME_KEY, System.currentTimeMillis());
        } else {
            preferences.putString(CRYPTO_USER_KEY, "");
        }
    }

    @Override
    public User getUser() {
        return getCrypto(User.class, CRYPTO_USER_KEY);
    }

    @Override
    public void saveNotify(Notify notify) {
        preferences.putString(NOTIFY_KEY, new Gson().toJson(notify));
        preferences.putLong(LAST_NOTIFY_UPDATE_TIME_KEY, System.currentTimeMillis());
    }

    @Override
    public Notify getNotify() {
        return new Gson().fromJson(preferences.getString(NOTIFY_KEY), Notify.class);
    }

    @Override
    public void saveHeader(String header) {
        preferences.putString(HEADER_KEY, header);
    }

    @Override
    public String getHeader() {
        return preferences.getString(HEADER_KEY);
    }

    @Override
    public void saveToken(String token) {
        preferences.putString(TOKEN_KEY, token);
    }

    @Override
    public String getToken() {
        return preferences.getString(TOKEN_KEY);
    }

    @Override
    public int isConfirm() {
        return preferences.getInt(CONFIRM_KEY);
    }

    @Override
    public void setConfirm(int isConfirm) {
        preferences.putInt(CONFIRM_KEY, isConfirm);
    }

    @Override
    public AuthType loadAuthType() {
        return AuthType.fromInt(preferences.getInt(AUTH_TYPE_KEY, -1));
    }

    @Override
    public void saveAuthType(AuthType authType) {
        preferences.putInt(AUTH_TYPE_KEY, AuthType.toInt(authType));
    }

    @Override
    public long loadLastOrdersUpdateTime() {
        return preferences.getLong(LAST_ORDERS_UPDATE_TIME_KEY);
    }

    @Override
    public void savePushToken(String pushToken) {
        preferences.putString(PUSH_TOKEN_KEY, pushToken);
    }

    @Override
    public boolean getFirstLoadingIsCompleted() {
        return preferences.getBoolean(FIRST_LOADING_IS_COMPLETED_KEY);
    }

    @Override
    public void setFirstLoadingIsCompleted(boolean firstLoadingIsCompleted) {
        preferences.putBoolean(FIRST_LOADING_IS_COMPLETED_KEY, firstLoadingIsCompleted);
    }

    @Override
    public String loadPushToken() {
        return preferences.getString(PUSH_TOKEN_KEY);
    }

    @Override
    public void clearWithout(String... fields) {
        Map<String, ?> allEntries = preferences.getAll();
        for (Map.Entry<String, ?> entry : allEntries.entrySet()) {
            boolean canDel = true;
            for (String field : fields) {
                if (field.equals(entry.getKey())) {
                    canDel = false;
                }
            }
            if (canDel) preferences.remove(entry.getKey());
        }
    }

    @Override
    public void saveContacts(Contacts contacts) {
        preferences.putString(CONTACTS_KEY, new Gson().toJson(contacts));
    }

    @Override
    public Contacts getContacts() {
        return new Gson().fromJson(preferences.getString(CONTACTS_KEY), Contacts.class);
    }

    @Override
    public void saveLastOrdersUpdateTime(long time) {
        preferences.putLong(LAST_ORDERS_UPDATE_TIME_KEY, time);
    }

    @Override
    public long loadLastProfileUpdateTime() {
        return preferences.getLong(LAST_PROFILE_UPDATE_TIME_KEY);
    }

    private void saveCrypto(Object obj, String KEY) {
        try {
            String jsonObj = new Gson().toJson(obj);
            String cryptoObj = cryptoProxy.encrypt(jsonObj);
            preferences.putString(KEY, cryptoObj);
        } catch (UnsupportedEncodingException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
    }

    private <T> T getCrypto(Class<T> classOfT, String KEY) {
        String cryptoObj = preferences.getString(KEY);
        if (cryptoObj.isEmpty()) return null;
        try {
            String jsonObj = cryptoProxy.decrypt(cryptoObj);
            return new Gson().fromJson(jsonObj, classOfT);
        } catch (IOException | IllegalBlockSizeException | BadPaddingException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean loadNeedChooseAppDialog() {
        return preferences.getBoolean(NEED_DONT_ASK_ANYMORE_KEY, true);
    }

    @Override
    public void saveNeedChooseAppDialog(boolean value) {
        preferences.putBoolean(NEED_DONT_ASK_ANYMORE_KEY, value);
    }

    @Override
    public String loadReferrer() {
        return preferences.getString(REFERRER_KEY);
    }

    @Override
    public void saveReferrer(String referrer) {
        preferences.putString(REFERRER_KEY, referrer);
    }

    @Override
    public void saveLastFaqUpdateTime(long time) {
        preferences.putLong(LAST_FAQ_UPDATE_TIME_KEY, time);
    }

    @Override
    public long loadLastFaqUpdateTime() {
        return preferences.getLong(LAST_FAQ_UPDATE_TIME_KEY);
    }

    @Override
    public void saveLastRulesUpdateTime(long time) {
        preferences.putLong(LAST_RULES_UPDATE_TIME_KEY, time);
    }

    @Override
    public long loadLastRulesUpdateTime() {
        return preferences.getLong(LAST_RULES_UPDATE_TIME_KEY);
    }

    @Override
    public void saveLaunchCount(int count) {
        preferences.putInt(LAUNCH_COUNT_KEY, count);
    }

    @Override
    public int loadLaunchCount() {
        return preferences.getInt(LAUNCH_COUNT_KEY);
    }

    @Override
    public void saveLastSlidersUpdateTime(long time) {
        preferences.putLong(LAST_SLIDERS_UPDATE_TIME_KEY, time);
    }

    @Override
    public long loadLastSlidersUpdateTime() {
        return preferences.getLong(LAST_SLIDERS_UPDATE_TIME_KEY);
    }
}
