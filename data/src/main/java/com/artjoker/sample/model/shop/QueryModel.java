package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.QueryContract;

@StorIOSQLiteType(table = "queries")
public class QueryModel {
    @StorIOSQLiteColumn(name = QueryContract.QueryColumns.ID, key = true)
    @Expose
    protected String id;
    @StorIOSQLiteColumn(name = QueryContract.QueryColumns.TITLE)
    @Expose
    protected String title;
    @StorIOSQLiteColumn(name = QueryContract.QueryColumns.QUERIES)
    @Expose
    protected String queries;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getQueries() {
        return queries;
    }

    public void setQueries(String queries) {
        this.queries = queries;
    }
}
