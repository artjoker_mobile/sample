package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.CouponContract;

import java.io.Serializable;

@StorIOSQLiteType(table = CouponContract.TABLE)
public class CouponModel implements Serializable{
    @StorIOSQLiteColumn(name = CouponContract.CouponColumns.ID, key = true)
    @Expose
    protected int id;
    @StorIOSQLiteColumn(name = CouponContract.CouponColumns.SHOP_ID)
    @Expose
    @SerializedName("shop_id")
    protected int shopId;
    @StorIOSQLiteColumn(name = CouponContract.CouponColumns.TITLE, key = true)
    @Expose
    protected String title;
    @StorIOSQLiteColumn(name = CouponContract.CouponColumns.DATE_END)
    @Expose
    @SerializedName("date_end")
    protected String dateEnd;
    @StorIOSQLiteColumn(name = CouponContract.CouponColumns.CODE)
    @Expose
    protected String code;
    @StorIOSQLiteColumn(name = CouponContract.CouponColumns.URL)
    @Expose
    protected String url;

    protected ShopModel shopModel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(String dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public ShopModel getShopModel() {
        return shopModel;
    }

    public void setShopModel(ShopModel shopModel) {
        this.shopModel = shopModel;
    }
}
