package com.artjoker.sample.smartysale.model.user;

public enum  Currency {
    RUB, UAH, USD, EUR
}
