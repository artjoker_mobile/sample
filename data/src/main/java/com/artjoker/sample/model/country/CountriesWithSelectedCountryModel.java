package com.artjoker.sample.smartysale.model.country;

import com.artjoker.sample.smartysale.api.models.responses.DetectCountryModel;

import java.util.List;

public class CountriesWithSelectedCountryModel {
    private List<CountryModel> countries;
    private DetectCountryModel detectedCountry;

    public CountriesWithSelectedCountryModel(List<CountryModel> countries, DetectCountryModel detectedCountry) {
        this.countries = countries;
        this.detectedCountry = detectedCountry;
    }

    public List<CountryModel> getCountries() {
        return countries;
    }

    public void setCountries(List<CountryModel> countries) {
        this.countries = countries;
    }

    public DetectCountryModel getDetectedCountry() {
        return detectedCountry;
    }

    public void setDetectedCountry(DetectCountryModel detectedCountry) {
        this.detectedCountry = detectedCountry;
    }
}
