package com.artjoker.sample.smartysale.model.profile;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MinimalProfileModel {

    @Expose
    private String name;

    @Expose
    @SerializedName("last_name")
    private String lastName;

    @Expose
    @SerializedName("birth_day")
    private int birthDay;

    @Expose
    @SerializedName("birth_month")
    private int birthMonth;

    @Expose
    @SerializedName("birth_year")
    private int birthYear;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(int birthDay) {
        this.birthDay = birthDay;
    }

    public int getBirthMonth() {
        return birthMonth;
    }

    public void setBirthMonth(int birthMonth) {
        this.birthMonth = birthMonth;
    }

    public int getBirthYear() {
        return birthYear;
    }

    public void setBirthYear(int birthYear) {
        this.birthYear = birthYear;
    }
}
