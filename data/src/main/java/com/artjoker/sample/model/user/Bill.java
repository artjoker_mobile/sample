package com.artjoker.sample.smartysale.model.user;

import android.support.annotation.DrawableRes;

public class Bill {
    private float amount;
    private Currency currency;
    @DrawableRes
    private int backgroundDrawable;

    public Bill(float amount, Currency currency) {
        this.amount = amount;
        this.currency = currency;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public void setBackgroundDrawable(int backgroundDrawable) {
        this.backgroundDrawable = backgroundDrawable;
    }

    public float getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public int getBackgroundDrawable() {
        return backgroundDrawable;
    }
}