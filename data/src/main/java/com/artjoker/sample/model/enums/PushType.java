package com.artjoker.sample.smartysale.model.enums;

public enum PushType {

    ORDER("order"),
    REFERRALS("referrals"),
    UNKNOWN("");

    private String typeName;

    PushType(String typeName) {
        this.typeName = typeName;
    }

    public static PushType findByName(String name) {
        for(PushType type : values()) {
            if(type.typeName.equals(name)) {
                return type;
            }
        }
        return UNKNOWN;
    }
}
