package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.ShopDetailsContract;

import java.io.Serializable;
import java.util.List;

@StorIOSQLiteType(table = ShopDetailsContract.TABLE)
public class ShopDetailsModel implements Serializable {
    @StorIOSQLiteColumn(name = ShopDetailsContract.ShopDetailsColumns.ID, key = true)
    @Expose
    protected int id;
    @StorIOSQLiteColumn(name = ShopDetailsContract.ShopDetailsColumns.DESC)
    @Expose
    protected String desc;
    @StorIOSQLiteColumn(name = ShopDetailsContract.ShopDetailsColumns.FULL_DESC)
    @Expose
    @SerializedName("full_desc")
    protected String fullDesc;
    @StorIOSQLiteColumn(name = ShopDetailsContract.ShopDetailsColumns.MY_ORDERS)
    @Expose
    @SerializedName("my_orders")
    protected int myOrders;
    @StorIOSQLiteColumn(name = ShopDetailsContract.ShopDetailsColumns.MY_REVIEWS)
    @Expose
    @SerializedName("my_reviews")
    protected int myReviews;
    @Expose
    protected List<CouponModel> coupons;
    @Expose
    protected List<ReviewModel> reviews;
    @Expose
    protected List<TariffModel> tariffs;
    @StorIOSQLiteColumn(name = ShopDetailsContract.ShopDetailsColumns.LAST_UPDATE_DB_MS)
    protected long lastUpdateDbMs;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getFullDesc() {
        return fullDesc;
    }

    public void setFullDesc(String fullDesc) {
        this.fullDesc = fullDesc;
    }

    public int getMyOrders() {
        return myOrders;
    }

    public void setMyOrders(int myOrders) {
        this.myOrders = myOrders;
    }

    public int getMyReviews() {
        return myReviews;
    }

    public void setMyReviews(int myReviews) {
        this.myReviews = myReviews;
    }

    public List<CouponModel> getCoupons() {
        return coupons;
    }

    public void setCoupons(List<CouponModel> coupons) {
        this.coupons = coupons;
    }

    public List<ReviewModel> getReviews() {
        return reviews;
    }

    public void setReviews(List<ReviewModel> reviews) {
        this.reviews = reviews;
    }

    public List<TariffModel> getTariffs() {
        return tariffs;
    }

    public void setTariffs(List<TariffModel> tariffs) {
        this.tariffs = tariffs;
    }

    public long getLastUpdateDbMs() {
        return lastUpdateDbMs;
    }

    public void setLastUpdateDbMs(long lastUpdateDbMs) {
        this.lastUpdateDbMs = lastUpdateDbMs;
    }
}
