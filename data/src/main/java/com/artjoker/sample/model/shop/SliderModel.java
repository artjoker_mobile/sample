package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.SliderContract;

import java.io.Serializable;

@StorIOSQLiteType(table = SliderContract.TABLE)
public class SliderModel implements Serializable {
    @Expose
    @StorIOSQLiteColumn(name = SliderContract.SliderColumns.POS, key = true)
    protected int pos;
    @Expose
    @StorIOSQLiteColumn(name = SliderContract.SliderColumns.IMAGE)
    protected String image;
    @Expose
    @StorIOSQLiteColumn(name = SliderContract.SliderColumns.NEED_AUTH)
    @SerializedName("need_auth")
    protected int needAuth;
    @Expose
    @StorIOSQLiteColumn(name = SliderContract.SliderColumns.ACTION)
    protected String action;
    @Expose
    @StorIOSQLiteColumn(name = SliderContract.SliderColumns.ACTION_ID)
    @SerializedName("action_id")
    protected int actionId;
    @Expose
    @StorIOSQLiteColumn(name = SliderContract.SliderColumns.CRC)
    protected long crc;
    @Expose
    protected SizeModel size;
    @StorIOSQLiteColumn(name = SliderContract.SliderColumns.SIZE_JSON)
    protected String sizeJson;

    public int getNeedAuth() {
        return needAuth;
    }

    public void setNeedAuth(int needAuth) {
        this.needAuth = needAuth;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public int getActionId() {
        return actionId;
    }

    public void setActionId(int actionId) {
        this.actionId = actionId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public long getCrc() {
        return crc;
    }

    public void setCrc(long crc) {
        this.crc = crc;
    }

    public SizeModel getSize() {
        return size;
    }

    public void setSize(SizeModel size) {
        this.size = size;
    }

    public String getSizeJson() {
        return sizeJson;
    }

    public void setSizeJson(String sizeJson) {
        this.sizeJson = sizeJson;
    }
}
