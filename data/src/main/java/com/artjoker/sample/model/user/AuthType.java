package com.artjoker.sample.smartysale.model.user;

public enum AuthType {
    GUEST,
    SOCIAL,
    LOGIN,
    NONE;

    public static AuthType fromInt(int x) {
        switch (x) {
            case 0:
                return GUEST;
            case 1:
                return SOCIAL;
            case 2:
                return LOGIN;
            default:
                return NONE;
        }
    }

    public static int toInt(AuthType authType) {
        switch (authType) {
            case GUEST:
                return 0;
            case SOCIAL:
                return 1;
            case LOGIN:
                return 2;
            default:
                return -1;
        }
    }
}
