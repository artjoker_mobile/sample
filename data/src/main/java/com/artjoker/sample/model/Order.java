package com.artjoker.sample.smartysale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.OrdersContract;
import com.artjoker.sample.smartysale.model.shop.ShopModel;

import java.io.Serializable;

@StorIOSQLiteType(table = OrdersContract.TABLE)
public class Order implements Serializable {

    @Expose
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.ID, key = true)
    protected int id;
    @Expose
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.DATE)
    protected String date;
    @Expose
    @SerializedName("shop_id")
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.SHOP_ID)
    protected int shopId;
    @Expose
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.STATUS)
    protected int status;
    @Expose
    @SerializedName("cashback")
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.CASH_BACK)
    protected String cashBack;
    @Expose
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.CURRENCY)
    protected String currency;
    @Expose
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.CONVERT)
    protected String convert;
    @Expose
    @StorIOSQLiteColumn(name = OrdersContract.OrderColumns.SENDED)
    protected int sended;

    private ShopModel shopModel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCashBack() {
        return cashBack;
    }

    public void setCashBack(String cashBack) {
        this.cashBack = cashBack;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getConvert() {
        return convert;
    }

    public void setConvert(String convert) {
        this.convert = convert;
    }

    public int getSended() {
        return sended;
    }

    public void setSended(int sended) {
        this.sended = sended;
    }

    public ShopModel getShopModel() {
        return shopModel;
    }

    public void setShopModel(ShopModel shopModel) {
        this.shopModel = shopModel;
    }
}
