package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.TariffContract;

import java.io.Serializable;

@StorIOSQLiteType(table = TariffContract.TABLE)
public class TariffModel implements Serializable {
    @StorIOSQLiteColumn(name = TariffContract.TariffColumns.ID, key = true)
    protected String id;
    @StorIOSQLiteColumn(name = TariffContract.TariffColumns.TITLE)
    @Expose
    protected String title;
    @StorIOSQLiteColumn(name = TariffContract.TariffColumns.TARIFF_TO)
    @Expose
    protected boolean to;
    @StorIOSQLiteColumn(name = TariffContract.TariffColumns.CASH_BACK)
    @Expose
    protected float cashback;
    @StorIOSQLiteColumn(name = TariffContract.TariffColumns.PERCENT)
    @Expose
    protected boolean percent;
    @StorIOSQLiteColumn(name = TariffContract.TariffColumns.TARIFF_ORDER)
    @Expose
    protected int order;
    @StorIOSQLiteColumn(name = TariffContract.TariffColumns.SHOP_ID)
    protected int shopId;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public boolean isTo() {
        return to;
    }

    public void setTo(boolean to) {
        this.to = to;
    }

    public float getCashback() {
        return cashback;
    }

    public void setCashback(float cashback) {
        this.cashback = cashback;
    }

    public boolean isPercent() {
        return percent;
    }

    public void setPercent(boolean percent) {
        this.percent = percent;
    }

    public int getOrder() {
        return order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }
}
