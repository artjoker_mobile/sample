package com.artjoker.sample.smartysale.model.enums;

public enum SliderActionType {

    SHOPS("shops"),
    REFERRAL("referral"),
    SHOP_VIEW("shop-view"),
    SHOP_GO("shop-go"),
    NEWS_VIEW("news-view"),
    UNKNOWN("");

    private String typeName;

    SliderActionType(String typeName) {
        this.typeName = typeName;
    }

    public static SliderActionType findByName(String name) {
        for(SliderActionType type : values()) {
            if(type.typeName.equals(name)) {
                return type;
            }
        }
        return UNKNOWN;
    }
}
