package com.artjoker.sample.smartysale.model;

import com.google.gson.annotations.Expose;

public class Notify {

    @Expose
    private int actions;
    @Expose
    private int newshops;
    @Expose
    private int coupons;
    @Expose
    private int sitenews;
    @Expose
    private int shopsnews;

    public enum Notifications  {
        ACTIONS("actions"),
        NEW_SHOPS("newshops"),
        COUPONS("coupons"),
        SITE_NEWS("sitenews"),
        SHOPS_NEWS("shopsnews");

        private final String text;

        Notifications(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public int getActions() {
        return actions;
    }

    public void setActions(int actions) {
        this.actions = actions;
    }

    public int getNewshops() {
        return newshops;
    }

    public void setNewshops(int newshops) {
        this.newshops = newshops;
    }

    public int getCoupons() {
        return coupons;
    }

    public void setCoupons(int coupons) {
        this.coupons = coupons;
    }

    public int getSitenews() {
        return sitenews;
    }

    public void setSitenews(int sitenews) {
        this.sitenews = sitenews;
    }

    public int getShopsnews() {
        return shopsnews;
    }

    public void setShopsnews(int shopsnews) {
        this.shopsnews = shopsnews;
    }
}
