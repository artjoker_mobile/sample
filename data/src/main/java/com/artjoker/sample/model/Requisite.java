package com.artjoker.sample.smartysale.model;

import com.google.gson.annotations.Expose;
import com.artjoker.sample.smartysale.api.models.responses.Response;

import java.io.Serializable;

public class Requisite extends Response implements Serializable {
    @Expose
    private long id;
    @Expose
    private String requisites;
    @Expose
    private int approved;
    @Expose
    private String provider;
    @Expose
    private String sum;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRequisites() {
        return requisites;
    }

    public void setRequisites(String requisites) {
        this.requisites = requisites;
    }

    public int getApproved() {
        return approved;
    }

    public void setApproved(int approved) {
        this.approved = approved;
    }

    public String getProvider() {
        return provider;
    }

    public void setProvider(String provider) {
        this.provider = provider;
    }

    public String getSum() {
        return sum;
    }

    public void setSum(String sum) {
        this.sum = sum;
    }
}
