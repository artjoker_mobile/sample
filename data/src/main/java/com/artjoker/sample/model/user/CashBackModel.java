package com.artjoker.sample.smartysale.model.user;

public class CashBackModel {
    private float cashBack;

    private String currency;

    private int symbolDrawableId;

    public CashBackModel(float cashBack, String currency, int symbolDrawableId) {
        this.cashBack = cashBack;
        this.currency = currency;
        this.symbolDrawableId = symbolDrawableId;
    }

    public float getCashBack() {
        return cashBack;
    }

    public void setCashBack(float cashBack) {
        this.cashBack = cashBack;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getSymbolDrawableId() {
        return symbolDrawableId;
    }

    public void setSymbolDrawableId(int symbolDrawableId) {
        this.symbolDrawableId = symbolDrawableId;
    }
}
