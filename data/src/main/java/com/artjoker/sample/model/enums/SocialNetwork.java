package com.artjoker.sample.smartysale.model.enums;

public enum SocialNetwork {
    FACEBOOK,
    VKONTAKTE,
    TWITTER,
    GOOGLE,
    ODNOKLASNIKI,
    MAILRU
}
