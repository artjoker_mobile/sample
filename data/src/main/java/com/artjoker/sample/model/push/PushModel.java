package com.artjoker.sample.smartysale.model.push;

import com.artjoker.sample.smartysale.model.enums.PushType;

import java.io.Serializable;

public class PushModel implements Serializable {

    private PushType type;

    private String title;

    private String body;

    private String data;

    public PushType getType() {
        return type;
    }

    public void setType(PushType type) {
        this.type = type;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}