package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.CategoryContract;

@StorIOSQLiteType(table = "categories")
public class CategoryModel {

    public static final int ALL_SHOPS_CATEGORY_ID = -1;

    @StorIOSQLiteColumn(name = CategoryContract.CategoryColumns.ID, key = true)
    @Expose
    protected int id;
    @StorIOSQLiteColumn(name = CategoryContract.CategoryColumns.TITLE)
    @Expose
    protected String title;
    @StorIOSQLiteColumn(name = CategoryContract.CategoryColumns.SHOPS_COUNT)
    @SerializedName("shops_count")
    @Expose
    protected int shopsCount;

    public CategoryModel() {
    }

    public CategoryModel(int id, String title) {
        this.id = id;
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getShopsCount() {
        return shopsCount;
    }

    public void setShopsCount(int shopsCount) {
        this.shopsCount = shopsCount;
    }
}
