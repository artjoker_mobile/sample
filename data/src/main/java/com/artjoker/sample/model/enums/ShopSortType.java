package com.artjoker.sample.smartysale.model.enums;

public enum ShopSortType {
    POPULARITY,
    NEW,
    DEDUCTION,
    CATALOG
}
