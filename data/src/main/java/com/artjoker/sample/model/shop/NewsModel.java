package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.NewsContract;

import java.io.Serializable;

@StorIOSQLiteType(table = NewsContract.TABLE)
public class NewsModel implements Serializable {
    @StorIOSQLiteColumn(name = NewsContract.NewsColumns.ID, key = true)
    @Expose
    protected int id;
    @StorIOSQLiteColumn(name = NewsContract.NewsColumns.TITLE)
    @Expose
    protected String title;
    @StorIOSQLiteColumn(name = NewsContract.NewsColumns.SHORT_TEXT_CLEAN)
    @SerializedName("short_text_clean")
    @Expose
    protected String shortTextClean;
    @StorIOSQLiteColumn(name = NewsContract.NewsColumns.DATE)
    @Expose
    protected String date;
    @StorIOSQLiteColumn(name = NewsContract.NewsColumns.IMAGE)
    @Expose
    protected String image;
    @StorIOSQLiteColumn(name = NewsContract.NewsColumns.PREV)
    @Expose
    protected int prev;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortTextClean() {
        return shortTextClean;
    }

    public void setShortTextClean(String shortTextClean) {
        this.shortTextClean = shortTextClean;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPrev() {
        return prev;
    }

    public void setPrev(int prev) {
        this.prev = prev;
    }
}
