package com.artjoker.sample.smartysale.model.enums;

public enum PayoutFilterType {
    ALL(-1),
    IN_PROGRESS(0),
    SUCCESS(1),
    REJECTED(2);

    private int id;

    PayoutFilterType(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }
}
