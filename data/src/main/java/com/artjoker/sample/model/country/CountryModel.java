package com.artjoker.sample.smartysale.model.country;

import com.google.gson.annotations.Expose;

public class CountryModel {

    @Expose
    private int pos;

    @Expose
    private String code;

    @Expose
    private String title;

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
