package com.artjoker.sample.smartysale.model;

import com.google.gson.annotations.Expose;

import java.io.Serializable;

public class ReferralModel implements Serializable {
    @Expose
    protected String name;

    @Expose
    protected String date;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
