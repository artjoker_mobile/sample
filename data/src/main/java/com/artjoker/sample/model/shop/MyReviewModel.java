package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.MyReviewContract;
import com.artjoker.sample.smartysale.db.contracts.ReviewContract;

import java.io.Serializable;

@StorIOSQLiteType(table = MyReviewContract.TABLE)
public class MyReviewModel implements Serializable {
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.ID, key = true)
    @Expose
    protected int id;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.SHOP_ID)
    @Expose
    @SerializedName("shop_id")
    protected int shopId;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.TEXT)
    @Expose
    protected String text;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.ANSWER)
    @Expose
    protected String answer;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.DATE)
    @Expose
    protected String date;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.AUTHOR)
    @Expose
    protected String author;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.AVATAR)
    @Expose
    protected String avatar;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.RATING)
    @Expose
    protected int rating;
    @StorIOSQLiteColumn(name = ReviewContract.ReviewColumns.MY)
    @Expose
    protected int my;

    private ShopModel shopModel;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getShopId() {
        return shopId;
    }

    public void setShopId(int shopId) {
        this.shopId = shopId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getMy() {
        return my;
    }

    public void setMy(int my) {
        this.my = my;
    }

    public ShopModel getShopModel() {
        return shopModel;
    }

    public void setShopModel(ShopModel shopModel) {
        this.shopModel = shopModel;
    }
}
