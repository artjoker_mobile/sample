package com.artjoker.sample.smartysale.model.shop;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.ShopContract;

import java.io.Serializable;

@StorIOSQLiteType(table = ShopContract.TABLE)
public class ShopModel implements Serializable {
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.ID, key = true)
    @Expose
    protected int id;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.TITLE)
    @Expose
    protected String title;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.LOGO)
    @Expose
    protected String logo;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.LOGO_SIZE)
    @Expose
    @SerializedName("logo_size")
    protected int logoSize;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.COUNTRY)
    @Expose
    protected int country;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.URL)
    @Expose
    protected String url;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.SLUG)
    @Expose
    protected String slug;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.DATE)
    @Expose
    protected String date;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.REVIEWS_COUNT)
    @Expose
    @SerializedName("reviews_count")
    protected int reviewsCount;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.COUPONS_COUNT)
    @Expose
    @SerializedName("coupons_count")
    protected int couponsCount;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.ON)
    @Expose
    protected int on;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.RATING)
    @Expose
    protected int rating;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.DAYS_HOLD)
    @Expose
    @SerializedName("days_hold")
    protected int daysHold;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.SEARCH)
    @Expose
    protected String search;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.SORT)
    @Expose
    protected float sort;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.CURRENCY)
    @Expose
    protected String currency;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.TARIFF_TO)
    @Expose
    @SerializedName("tariff_to")
    protected int tariffTo;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.TARIFF_CASHBACK)
    @Expose
    @SerializedName("tariff_cashback")
    protected String tariffCashback;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.TARIFF_PERCENT)
    @Expose
    @SerializedName("tariff_percent")
    protected int tariffPercent;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.ACTION)
    @Expose
    protected float action;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.FAVORITE)
    @Expose
    protected int favorite;
    @Expose
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.FILTER)
    protected String filter;
    @Expose
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.DEFAULT_CATEGORY)
    @SerializedName("default_category")
    protected Integer defaultCategory;
    @Expose
    @SerializedName("categories_ids")
    protected int[] categoriesIds;
    @Expose
    @SerializedName("queries_ids")
    protected int[] queriesIds;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.CATEGORIES_IDS_STR)
    protected String categoriesIdsStr;
    @StorIOSQLiteColumn(name = ShopContract.ShopColumns.QUERIES_IDS_STR)
    protected String queriesIdsStr;

    private String categoryName;

    private float cashBackPromo;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public int getLogoSize() {
        return logoSize;
    }

    public void setLogoSize(int logoSize) {
        this.logoSize = logoSize;
    }

    public int getCountry() {
        return country;
    }

    public void setCountry(int country) {
        this.country = country;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(int reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public int getCouponsCount() {
        return couponsCount;
    }

    public void setCouponsCount(int couponsCount) {
        this.couponsCount = couponsCount;
    }

    public int getOn() {
        return on;
    }

    public void setOn(int on) {
        this.on = on;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getDaysHold() {
        return daysHold;
    }

    public void setDaysHold(int daysHold) {
        this.daysHold = daysHold;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        this.search = search;
    }

    public float getSort() {
        return sort;
    }

    public void setSort(float sort) {
        this.sort = sort;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getTariffTo() {
        return tariffTo;
    }

    public void setTariffTo(int tariffTo) {
        this.tariffTo = tariffTo;
    }

    public String getTariffCashback() {
        return tariffCashback;
    }

    public void setTariffCashback(String tariffCashback) {
        this.tariffCashback = tariffCashback;
    }

    public int getTariffPercent() {
        return tariffPercent;
    }

    public void setTariffPercent(int tariffPercent) {
        this.tariffPercent = tariffPercent;
    }

    public float getAction() {
        return action;
    }

    public void setAction(float action) {
        this.action = action;
    }

    public int[] getCategoriesIds() {
        return categoriesIds;
    }

    public void setCategoriesIds(int[] categoriesIds) {
        this.categoriesIds = categoriesIds;
    }

    public int[] getQueriesIds() {
        return queriesIds;
    }

    public void setQueriesIds(int[] queriesIds) {
        this.queriesIds = queriesIds;
    }

    public int getFavorite() {
        return favorite;
    }

    public void setFavorite(int favorite) {
        this.favorite = favorite;
    }

    public String getFilter() {
        return filter;
    }

    public void setFilter(String filter) {
        this.filter = filter;
    }

    public Integer getDefaultCategory() {
        return defaultCategory;
    }

    public void setDefaultCategory(Integer defaultCategory) {
        this.defaultCategory = defaultCategory;
    }

    public String getCategoriesIdsStr() {
        return categoriesIdsStr;
    }

    public void setCategoriesIdsStr(String categoriesIdsStr) {
        this.categoriesIdsStr = categoriesIdsStr;
    }

    public String getQueriesIdsStr() {
        return queriesIdsStr;
    }

    public void setQueriesIdsStr(String queriesIdsStr) {
        this.queriesIdsStr = queriesIdsStr;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public float getCashBackPromo() {
        return cashBackPromo;
    }

    public void setCashBackPromo(float cashBackPromo) {
        this.cashBackPromo = cashBackPromo;
    }
}
