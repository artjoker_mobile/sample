package com.artjoker.sample.smartysale.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.PromoCodeContract;

import java.io.Serializable;
import java.util.Map;

@StorIOSQLiteType(table = PromoCodeContract.TABLE)
public class PromoCodeModel implements Serializable {

    @Expose
    @StorIOSQLiteColumn(name = PromoCodeContract.PromoCodeColumns.ID, key = true)
    protected int id;
    @Expose
    @StorIOSQLiteColumn(name = PromoCodeContract.PromoCodeColumns.CODE)
    protected String code;
    @Expose
    @StorIOSQLiteColumn(name = PromoCodeContract.PromoCodeColumns.TITLE)
    protected String title;
    @Expose
    @SerializedName("start_date")
    @StorIOSQLiteColumn(name = PromoCodeContract.PromoCodeColumns.START_DATE)
    protected String startDate;
    @Expose
    @SerializedName("end_date")
    @StorIOSQLiteColumn(name = PromoCodeContract.PromoCodeColumns.END_DATE)
    protected String endDate;
    @Expose
    @StorIOSQLiteColumn(name = PromoCodeContract.PromoCodeColumns.ALL)
    protected float all;
    @Expose
    protected Map<String, Float> conditions;
    @StorIOSQLiteColumn(name = PromoCodeContract.PromoCodeColumns.CONDITIONS)
    protected String conditionsStr;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public float getAll() {
        return all;
    }

    public void setAll(float all) {
        this.all = all;
    }

    public Map<String, Float> getConditions() {
        return conditions;
    }

    public void setConditions(Map<String, Float> conditions) {
        this.conditions = conditions;
    }

    public String getConditionsStr() {
        return conditionsStr;
    }

    public void setConditionsStr(String conditionsStr) {
        this.conditionsStr = conditionsStr;
    }
}
