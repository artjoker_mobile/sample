package com.artjoker.sample.smartysale.model.user;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.artjoker.sample.smartysale.model.profile.MinimalProfileModel;
import com.artjoker.sample.smartysale.model.profile.UpdateProfileModel;

public class User extends MinimalProfileModel {

    @Expose
    private long id;
    @Expose
    private String uah;
    @Expose
    private String rub;
    @Expose
    private String usd;
    @Expose
    private String eur;
    @Expose
    private int sex;
    @Expose
    private String email;
    @Expose
    private String phone;
    @Expose
    private int payouts;
    @Expose
    @SerializedName("payouts_total")
    private int payoutsTotal;
    @Expose
    private String avatar;
    @Expose
    @SerializedName("ref_groups_count")
    private int refGroupsCount;
    @Expose
    @SerializedName("ref_level")
    private int refLevel;
    @Expose
    @SerializedName("ref_remain")
    private int refRemain;
    @Expose
    @SerializedName("ref_percent")
    private int refPercent;
    @Expose
    @SerializedName("ref_percent_next")
    private int refPercentNext;
    @Expose
    @SerializedName("referrals_count")
    private int referralsCount;
    @Expose
    @SerializedName("all_referrals_count")
    private int allReferralsCount;
    @Expose
    @SerializedName("bonus_friends")
    private int bonusFriends;
    @Expose
    @SerializedName("approved_bonus_friends")
    private int approvedBonusFriends;

    private int avatarRandomParameter;

    public void init(User user) {
        setId(user.getId());

        setUah(user.getUah());
        setRub(user.getRub());
        setUsd(user.getUsd());
        setEur(user.getEur());

        setSex(user.getSex());
        setEmail(user.getEmail());
        setPhone(user.getPhone());
        setPayouts(user.getPayouts());
        setPayoutsTotal(user.getPayoutsTotal());

        setAvatar(user.getAvatar());

        setRefGroupsCount(user.getRefGroupsCount());
        setRefLevel(user.getRefLevel());
        setRefRemain(user.getRefRemain());
        setRefPercent(user.getRefPercent());
        setRefPercentNext(user.getRefPercentNext());
        setReferralsCount(user.getReferralsCount());
        setAllReferralsCount(user.getAllReferralsCount());
        setBonusFriends(user.getBonusFriends());
        setApprovedBonusFriends(user.getApprovedBonusFriends());

        setName(user.getName());
        setLastName(user.getLastName());
        setBirthDay(user.getBirthDay());
        setBirthMonth(user.getBirthMonth());
        setBirthYear(user.getBirthYear());
    }

    public void init(UpdateProfileModel updateProfileModel) {
        setName(updateProfileModel.getName());
        setLastName(updateProfileModel.getLastName());
        setBirthDay(updateProfileModel.getBirthDay());
        setBirthMonth(updateProfileModel.getBirthMonth());
        setBirthYear(updateProfileModel.getBirthYear());
        setSex(updateProfileModel.getSex());
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUah() {
        return uah;
    }

    public void setUah(String uah) {
        this.uah = uah;
    }

    public String getRub() {
        return rub;
    }

    public void setRub(String rub) {
        this.rub = rub;
    }

    public String getUsd() {
        return usd;
    }

    public void setUsd(String usd) {
        this.usd = usd;
    }

    public String getEur() {
        return eur;
    }

    public void setEur(String eur) {
        this.eur = eur;
    }

    public int getSex() {
        return sex;
    }

    public void setSex(int sex) {
        this.sex = sex;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public int getPayouts() {
        return payouts;
    }

    public void setPayouts(int payouts) {
        this.payouts = payouts;
    }

    public int getPayoutsTotal() {
        return payoutsTotal;
    }

    public void setPayoutsTotal(int payoutsTotal) {
        this.payoutsTotal = payoutsTotal;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public int getRefGroupsCount() {
        return refGroupsCount;
    }

    public void setRefGroupsCount(int refGroupsCount) {
        this.refGroupsCount = refGroupsCount;
    }

    public int getRefLevel() {
        return refLevel;
    }

    public void setRefLevel(int refLevel) {
        this.refLevel = refLevel;
    }

    public int getRefRemain() {
        return refRemain;
    }

    public void setRefRemain(int refRemain) {
        this.refRemain = refRemain;
    }

    public int getRefPercent() {
        return refPercent;
    }

    public void setRefPercent(int refPercent) {
        this.refPercent = refPercent;
    }

    public int getRefPercentNext() {
        return refPercentNext;
    }

    public void setRefPercentNext(int refPercentNext) {
        this.refPercentNext = refPercentNext;
    }

    public int getReferralsCount() {
        return referralsCount;
    }

    public void setReferralsCount(int referralsCount) {
        this.referralsCount = referralsCount;
    }

    public int getAllReferralsCount() {
        return allReferralsCount;
    }

    public void setAllReferralsCount(int allReferralsCount) {
        this.allReferralsCount = allReferralsCount;
    }

    public int getBonusFriends() {
        return bonusFriends;
    }

    public void setBonusFriends(int bonusFriends) {
        this.bonusFriends = bonusFriends;
    }

    public int getApprovedBonusFriends() {
        return approvedBonusFriends;
    }

    public void setApprovedBonusFriends(int approvedBonusFriends) {
        this.approvedBonusFriends = approvedBonusFriends;
    }

    public int getAvatarRandomParameter() {
        return avatarRandomParameter;
    }

    public void setAvatarRandomParameter(int avatarRandomParameter) {
        this.avatarRandomParameter = avatarRandomParameter;
    }
}
