package com.artjoker.sample.smartysale.model;

import com.google.gson.annotations.Expose;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.FaqContract;

@StorIOSQLiteType(table = FaqContract.TABLE)
public class FaqModel {

    @Expose
    @StorIOSQLiteColumn(name = FaqContract.FaqColumns.ID, key = true)
    protected int id;

    @Expose
    @StorIOSQLiteColumn(name = FaqContract.FaqColumns.QUESTION)
    protected String question;

    @Expose
    @StorIOSQLiteColumn(name = FaqContract.FaqColumns.ANSWER)
    protected String answer;

    @Expose
    @StorIOSQLiteColumn(name = FaqContract.FaqColumns.POS)
    protected int pos;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public int getPos() {
        return pos;
    }

    public void setPos(int pos) {
        this.pos = pos;
    }
}
