package com.artjoker.sample.smartysale.model;

import com.google.gson.annotations.Expose;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteColumn;
import com.pushtorefresh.storio.sqlite.annotations.StorIOSQLiteType;
import com.artjoker.sample.smartysale.db.contracts.RulesContract;

@StorIOSQLiteType(table = RulesContract.TABLE)
public class RuleModel {

    @Expose
    @StorIOSQLiteColumn(name = RulesContract.RulesColumns.NUMBER, key = true)
    protected int number;

    @Expose
    @StorIOSQLiteColumn(name = RulesContract.RulesColumns.TITLE)
    protected String title;

    @Expose
    @StorIOSQLiteColumn(name = RulesContract.RulesColumns.PARAGRAPHS)
    protected String paragraphs;

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getParagraphs() {
        return paragraphs;
    }

    public void setParagraphs(String paragraphs) {
        this.paragraphs = paragraphs;
    }
}