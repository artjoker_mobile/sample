package com.artjoker.sample;

import com.artjoker.sample.smartysale.api.models.responses.ShopsResponse;
import com.artjoker.sample.smartysale.model.shop.QueryModel;
import com.artjoker.sample.smartysale.model.shop.ShopModel;

import java.util.List;

public class SessionStorage {

    private ShopsResponse shopsResponse;

    public ShopModel getShopById(int id) {
        if (shopsResponse != null && shopsResponse.getShops() != null) {
            for (ShopModel model : shopsResponse.getShops()) {
                if (model.getId() == id) {
                    return model;
                }
            }
        }
        return null;
    }

    public List<ShopModel> getShops() {
        return shopsResponse.getShops();
    }

    public List<QueryModel> getQueries() {
        return shopsResponse.getQueries();
    }

    public ShopsResponse getShopsResponse() {
        return shopsResponse;
    }

    public void setShopsResponse(ShopsResponse shopsResponse) {
        this.shopsResponse = shopsResponse;
    }
}
