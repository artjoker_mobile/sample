package com.artjoker.sample.smartysale.db.resolvers;

import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.artjoker.sample.smartysale.db.contracts.CouponContract;
import com.artjoker.sample.smartysale.db.contracts.ReviewContract;
import com.artjoker.sample.smartysale.db.contracts.TariffContract;
import com.artjoker.sample.smartysale.model.shop.CouponModel;
import com.artjoker.sample.smartysale.model.shop.CouponModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.ReviewModel;
import com.artjoker.sample.smartysale.model.shop.ReviewModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.TariffModel;
import com.artjoker.sample.smartysale.model.shop.TariffModelStorIOSQLitePutResolver;

public class ShopDetailsPutResolver extends ShopDetailsModelStorIOSQLitePutResolver {

    private StorIOSQLite storIOSQLite;
    private CouponModelStorIOSQLitePutResolver couponModelStorIOSQLitePutResolver;
    private ReviewModelStorIOSQLitePutResolver reviewModelStorIOSQLitePutResolver;
    private TariffModelStorIOSQLitePutResolver tariffModelStorIOSQLitePutResolver;

    public ShopDetailsPutResolver(StorIOSQLite storIOSQLite) {
        this.storIOSQLite = storIOSQLite;
        couponModelStorIOSQLitePutResolver = new CouponModelStorIOSQLitePutResolver();
        reviewModelStorIOSQLitePutResolver = new ReviewModelStorIOSQLitePutResolver();
        tariffModelStorIOSQLitePutResolver = new TariffModelStorIOSQLitePutResolver();
    }

    @NonNull
    @Override
    public PutResult performPut(@NonNull StorIOSQLite storIOSQLite, @NonNull ShopDetailsModel shopDetails) {
        putCoupons(storIOSQLite, shopDetails);
        putReviews(storIOSQLite, shopDetails);
        putTariffs(storIOSQLite, shopDetails);
        return super.performPut(storIOSQLite, shopDetails);
    }

    private void putCoupons(StorIOSQLite storIOSQLite, ShopDetailsModel shopDetails) {
        deleteCoupons(shopDetails);
        for (CouponModel coupon : shopDetails.getCoupons()) {
            couponModelStorIOSQLitePutResolver.performPut(storIOSQLite, coupon);
        }
    }

    private void putReviews(StorIOSQLite storIOSQLite, ShopDetailsModel shopDetails) {
        deleteReviews(shopDetails);
        for (ReviewModel review : shopDetails.getReviews()) {
            reviewModelStorIOSQLitePutResolver.performPut(storIOSQLite, review);
        }
    }

    private void putTariffs(StorIOSQLite storIOSQLite, ShopDetailsModel shopDetails) {
        deleteTariffs(shopDetails);
        for (TariffModel tariff : shopDetails.getTariffs()) {
            tariffModelStorIOSQLitePutResolver.performPut(storIOSQLite, tariff);
        }
    }

    private void deleteCoupons(ShopDetailsModel shopDetails) {
        storIOSQLite.lowLevel().delete(DeleteQuery.builder()
                .table(CouponContract.TABLE)
                .where(CouponContract.CouponColumns.SHOP_ID + " = ?")
                .whereArgs(shopDetails.getId())
                .build());
    }

    private void deleteReviews(ShopDetailsModel shopDetails) {
        storIOSQLite.lowLevel().delete(DeleteQuery.builder()
                .table(ReviewContract.TABLE)
                .where(ReviewContract.ReviewColumns.SHOP_ID + " = ?")
                .whereArgs(shopDetails.getId())
                .build());
    }

    private void deleteTariffs(ShopDetailsModel shopDetails) {
        storIOSQLite.lowLevel().delete(DeleteQuery.builder()
                .table(TariffContract.TABLE)
                .where(TariffContract.TariffColumns.SHOP_ID + " = ?")
                .whereArgs(shopDetails.getId())
                .build());
    }
}
