package com.artjoker.sample.smartysale.db;

import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.artjoker.sample.smartysale.api.models.responses.ShopsResponse;
import com.artjoker.sample.smartysale.model.FaqModel;
import com.artjoker.sample.smartysale.model.Order;
import com.artjoker.sample.smartysale.model.PromoCodeModel;
import com.artjoker.sample.smartysale.model.RuleModel;
import com.artjoker.sample.smartysale.model.shop.MyReviewModel;
import com.artjoker.sample.smartysale.model.shop.NewsModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;
import com.artjoker.sample.smartysale.model.shop.ShopModel;
import com.artjoker.sample.smartysale.model.shop.SliderModel;

import java.util.List;

import io.reactivex.Observable;

public interface DbStorage {

    Observable<ShopsResponse> loadShopsResponse();
    Observable<ShopsResponse> saveShopsResponse(ShopsResponse shopsResponse);

    Observable<PutResult> putShop(ShopModel shop);

    Observable<ShopDetailsModel> putShopDetails(ShopDetailsModel shopDetailsModel);
    Observable<ShopDetailsModel> getShopDetails(int shopId);

    Observable<List<NewsModel>> saveNews(List<NewsModel> news);
    Observable<List<NewsModel>> loadNews();

    Observable<List<Order>> saveOrders(List<Order> orders);
    Observable<List<Order>> loadOrders();

    Observable<List<MyReviewModel>> saveMyReviews(List<MyReviewModel> myReviews);
    Observable<List<MyReviewModel>> loadMyReviews();

    Observable<List<PromoCodeModel>> savePromoCodes(List<PromoCodeModel> promoCodes);
    Observable<List<PromoCodeModel>> loadPromoCodes();

    Observable<List<FaqModel>> saveFaq(List<FaqModel> faqModels);
    Observable<List<FaqModel>> loadFaq();

    Observable<List<RuleModel>> saveRules(List<RuleModel> ruleModels);
    Observable<List<RuleModel>> loadRules();

    Observable<List<SliderModel>> saveSliders(List<SliderModel> slider);
    Observable<List<SliderModel>> loadSliders();
}
