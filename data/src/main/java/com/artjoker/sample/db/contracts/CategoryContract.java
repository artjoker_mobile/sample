package com.artjoker.sample.smartysale.db.contracts;


public interface CategoryContract {
    String TABLE = "categories";

    interface CategoryColumns {
        String ID = "id";

        String TITLE = "title";

        String SHOPS_COUNT = "shopsCount";
    }
}
