package com.artjoker.sample.smartysale.db.contracts;


public interface QueryContract {
    String TABLE = "queries";

    interface QueryColumns {
        String ID = "id";

        String TITLE = "title";

        String QUERIES = "queries";
    }
}
