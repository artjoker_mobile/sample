package com.artjoker.sample.smartysale.db.tables;

import android.support.annotation.NonNull;

import com.artjoker.sample.smartysale.db.contracts.ShopContract;
import com.artjoker.sample.smartysale.db.tables.utils.TableTypes;
import com.artjoker.sample.smartysale.db.tables.utils.TableUtils;

public class ShopTable {

    @NonNull
    public static String create() {
        return TableUtils.createTable(ShopContract.TABLE, ShopContract.ShopColumns.ID + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.TITLE + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.LOGO + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.LOGO_SIZE + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.COUNTRY + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.URL + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.SLUG + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.DATE + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.REVIEWS_COUNT + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.COUPONS_COUNT + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.ON + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.RATING + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.DAYS_HOLD + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.SEARCH + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.SORT + " " + TableTypes.REAL + ", "
                + ShopContract.ShopColumns.CURRENCY + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.TARIFF_TO + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.TARIFF_CASHBACK + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.TARIFF_PERCENT + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.ACTION + " " + TableTypes.REAL + ", "
                + ShopContract.ShopColumns.FAVORITE + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.FILTER + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.DEFAULT_CATEGORY + " " + TableTypes.INTEGER + ", "
                + ShopContract.ShopColumns.CATEGORIES_IDS_STR + " " + TableTypes.TEXT + ", "
                + ShopContract.ShopColumns.QUERIES_IDS_STR + " " + TableTypes.TEXT);
    }

    @NonNull
    public static String drop() {
        return TableUtils.dropTable(ShopContract.TABLE);
    }

}
