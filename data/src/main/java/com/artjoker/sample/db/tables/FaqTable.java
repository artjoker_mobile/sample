package com.artjoker.sample.smartysale.db.tables;

import android.support.annotation.NonNull;

import com.artjoker.sample.smartysale.db.contracts.FaqContract;
import com.artjoker.sample.smartysale.db.tables.utils.TableTypes;
import com.artjoker.sample.smartysale.db.tables.utils.TableUtils;

public class FaqTable {

    @NonNull
    public static String create() {
        return TableUtils.createTable(FaqContract.TABLE, FaqContract.FaqColumns.ID + " " + TableTypes.INTEGER + ", "
                + FaqContract.FaqColumns.QUESTION + " " + TableTypes.TEXT + ", "
                + FaqContract.FaqColumns.ANSWER + " " + TableTypes.TEXT + ", "
                + FaqContract.FaqColumns.POS + " " + TableTypes.INTEGER);
    }

    @NonNull
    public static String drop() {
        return TableUtils.dropTable(FaqContract.TABLE);
    }

}
