package com.artjoker.sample.smartysale.db.contracts;

public interface ReviewContract {
    String TABLE = "reviews";

    interface ReviewColumns {
        String ID = "id";

        String SHOP_ID = "shopId";

        String TEXT = "text";

        String ANSWER = "answer";

        String DATE = "date";

        String AUTHOR = "author";

        String AVATAR = "avatar";

        String RATING = "rating";

        String MY = "my";
    }
}
