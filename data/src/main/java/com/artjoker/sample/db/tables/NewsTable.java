package com.artjoker.sample.smartysale.db.tables;

import android.support.annotation.NonNull;

import com.artjoker.sample.smartysale.db.contracts.NewsContract;
import com.artjoker.sample.smartysale.db.tables.utils.TableTypes;
import com.artjoker.sample.smartysale.db.tables.utils.TableUtils;

public class NewsTable {

    @NonNull
    public static String create() {
        return TableUtils.createTable(NewsContract.TABLE, NewsContract.NewsColumns.ID + " " + TableTypes.INTEGER + ", "
                + NewsContract.NewsColumns.TITLE + " " + TableTypes.TEXT + ", "
                + NewsContract.NewsColumns.SHORT_TEXT_CLEAN + " " + TableTypes.TEXT + ", "
                + NewsContract.NewsColumns.DATE + " " + TableTypes.TEXT + ", "
                + NewsContract.NewsColumns.IMAGE + " " + TableTypes.TEXT + ", "
                + NewsContract.NewsColumns.PREV + " " + TableTypes.INTEGER);
    }

    @NonNull
    public static String drop() {
        return TableUtils.dropTable(NewsContract.TABLE);
    }

}
