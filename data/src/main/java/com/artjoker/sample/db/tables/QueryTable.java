package com.artjoker.sample.smartysale.db.tables;

import android.support.annotation.NonNull;

import com.artjoker.sample.smartysale.db.contracts.QueryContract;
import com.artjoker.sample.smartysale.db.tables.utils.TableTypes;
import com.artjoker.sample.smartysale.db.tables.utils.TableUtils;

public class QueryTable {

    @NonNull
    public static String create() {
        return TableUtils.createTable(QueryContract.TABLE,
                QueryContract.QueryColumns.ID + " " + TableTypes.INTEGER + ", "
                + QueryContract.QueryColumns.TITLE + " " + TableTypes.TEXT + ", "
                + QueryContract.QueryColumns.QUERIES + " " + TableTypes.TEXT);
    }

    @NonNull
    public static String drop() {
        return TableUtils.dropTable(QueryContract.TABLE);
    }

}
