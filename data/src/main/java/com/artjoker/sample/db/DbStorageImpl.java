package com.artjoker.sample.smartysale.db;


import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.operations.put.PutResult;
import com.pushtorefresh.storio.sqlite.queries.DeleteQuery;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.artjoker.sample.smartysale.api.models.responses.ShopsResponse;
import com.artjoker.sample.smartysale.db.contracts.CategoryContract;
import com.artjoker.sample.smartysale.db.contracts.FaqContract;
import com.artjoker.sample.smartysale.db.contracts.MyReviewContract;
import com.artjoker.sample.smartysale.db.contracts.NewsContract;
import com.artjoker.sample.smartysale.db.contracts.OrdersContract;
import com.artjoker.sample.smartysale.db.contracts.PromoCodeContract;
import com.artjoker.sample.smartysale.db.contracts.QueryContract;
import com.artjoker.sample.smartysale.db.contracts.RulesContract;
import com.artjoker.sample.smartysale.db.contracts.ShopContract;
import com.artjoker.sample.smartysale.db.contracts.ShopDetailsContract;
import com.artjoker.sample.smartysale.db.contracts.SliderContract;
import com.artjoker.sample.smartysale.db.resolvers.PromoCodeGetResolver;
import com.artjoker.sample.smartysale.db.resolvers.PromoCodePutResolver;
import com.artjoker.sample.smartysale.db.resolvers.ShopDetailsGetResolver;
import com.artjoker.sample.smartysale.db.resolvers.ShopDetailsPutResolver;
import com.artjoker.sample.smartysale.db.resolvers.ShopGetResolver;
import com.artjoker.sample.smartysale.db.resolvers.ShopPutResolver;
import com.artjoker.sample.smartysale.db.resolvers.SlidersGetResolver;
import com.artjoker.sample.smartysale.db.resolvers.SlidersPutResolver;
import com.artjoker.sample.smartysale.model.FaqModel;
import com.artjoker.sample.smartysale.model.Order;
import com.artjoker.sample.smartysale.model.PromoCodeModel;
import com.artjoker.sample.smartysale.model.RuleModel;
import com.artjoker.sample.smartysale.model.shop.CategoryModel;
import com.artjoker.sample.smartysale.model.shop.MyReviewModel;
import com.artjoker.sample.smartysale.model.shop.NewsModel;
import com.artjoker.sample.smartysale.model.shop.QueryModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;
import com.artjoker.sample.smartysale.model.shop.ShopModel;
import com.artjoker.sample.smartysale.model.shop.SliderModel;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

public class DbStorageImpl implements DbStorage {

    @Inject
    protected StorIOSQLite database;

    @Inject
    public DbStorageImpl() {
    }

    public Observable<ShopsResponse> loadShopsResponse() {
        return Observable.create(new ObservableOnSubscribe<ShopsResponse>() {
            @Override
            public void subscribe(ObservableEmitter<ShopsResponse> e) throws Exception {
                List<ShopModel> shops = database.get().listOfObjects(ShopModel.class)
                        .withQuery(Query.builder().table(ShopContract.TABLE).build())
                        .withGetResolver(new ShopGetResolver())
                        .prepare()
                        .executeAsBlocking();
                List<CategoryModel> categories = database.get().listOfObjects(CategoryModel.class)
                        .withQuery(Query.builder().table(CategoryContract.TABLE).build())
                        .prepare()
                        .executeAsBlocking();
                List<QueryModel> queries = database.get().listOfObjects(QueryModel.class)
                        .withQuery(Query.builder().table(QueryContract.TABLE).build())
                        .prepare()
                        .executeAsBlocking();

                ShopsResponse response = new ShopsResponse();
                response.setShops(shops);
                response.setCategories(categories);
                response.setQueries(queries);
                e.onNext(response);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<ShopsResponse> saveShopsResponse(ShopsResponse shopsResponse) {
        return Observable.create(new ObservableOnSubscribe<ShopsResponse>() {
            @Override
            public void subscribe(ObservableEmitter<ShopsResponse> e) throws Exception {
                database.lowLevel().beginTransaction();
                try {
                    database.delete().byQuery(DeleteQuery.builder().table(ShopContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(shopsResponse.getShops()).withPutResolver(new ShopPutResolver()).prepare().executeAsBlocking();
                    database.delete().byQuery(DeleteQuery.builder().table(CategoryContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(shopsResponse.getCategories()).prepare().executeAsBlocking();
                    database.delete().byQuery(DeleteQuery.builder().table(QueryContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(shopsResponse.getQueries()).prepare().executeAsBlocking();
                    database.lowLevel().setTransactionSuccessful();
                } finally {
                    database.lowLevel().endTransaction();
                }
                e.onNext(shopsResponse);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<PutResult> putShop(ShopModel shop) {
        return Observable.create(new ObservableOnSubscribe<PutResult>() {
            @Override
            public void subscribe(ObservableEmitter<PutResult> e) throws Exception {
                PutResult putResult = database.put().object(shop).withPutResolver(new ShopPutResolver()).prepare().executeAsBlocking();
                e.onNext(putResult);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<ShopDetailsModel> putShopDetails(ShopDetailsModel shopDetails) {
        return Observable.create(new ObservableOnSubscribe<ShopDetailsModel>() {
            @Override
            public void subscribe(ObservableEmitter<ShopDetailsModel> e) throws Exception {
                shopDetails.setLastUpdateDbMs(System.currentTimeMillis());
                database.put().object(shopDetails).withPutResolver(new ShopDetailsPutResolver(database)).prepare().executeAsBlocking();
                e.onNext(shopDetails);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<ShopDetailsModel> getShopDetails(int shopId) {
        return Observable.create(new ObservableOnSubscribe<ShopDetailsModel>() {
            @Override
            public void subscribe(ObservableEmitter<ShopDetailsModel> e) throws Exception {
                ShopDetailsModel details = database.get()
                        .object(ShopDetailsModel.class)
                        .withQuery(Query.builder()
                                .table(ShopDetailsContract.TABLE)
                                .where(ShopDetailsContract.ShopDetailsColumns.ID + " = ?")
                                .whereArgs(shopId)
                                .build())
                        .withGetResolver(new ShopDetailsGetResolver(database))
                        .prepare()
                        .executeAsBlocking();

                if (details == null) {
                    details = new ShopDetailsModel();
                }
                e.onNext(details);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<NewsModel>> saveNews(List<NewsModel> news) {
        return Observable.create(new ObservableOnSubscribe<List<NewsModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<NewsModel>> e) throws Exception {
                database.put().objects(news).prepare().executeAsBlocking();
                e.onNext(news);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<NewsModel>> loadNews() {
        return Observable.create(new ObservableOnSubscribe<List<NewsModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<NewsModel>> e) throws Exception {
                List<NewsModel> news = database.get().listOfObjects(NewsModel.class)
                        .withQuery(Query.builder().table(NewsContract.TABLE).build())
                        .prepare()
                        .executeAsBlocking();
                e.onNext(news);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<Order>> saveOrders(List<Order> orders) {
        return Observable.create(new ObservableOnSubscribe<List<Order>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Order>> e) throws Exception {
                database.put().objects(orders)
                        .prepare()
                        .executeAsBlocking();
                e.onNext(orders);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<Order>> loadOrders() {
        return Observable.create(new ObservableOnSubscribe<List<Order>>() {
            @Override
            public void subscribe(ObservableEmitter<List<Order>> e) throws Exception {
                List<Order> orders = database.get()
                        .listOfObjects(Order.class)
                        .withQuery(Query.builder().table(OrdersContract.TABLE).build())
                        .prepare()
                        .executeAsBlocking();
                e.onNext(orders);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<MyReviewModel>> saveMyReviews(List<MyReviewModel> myReviews) {
        return Observable.create(new ObservableOnSubscribe<List<MyReviewModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<MyReviewModel>> e) throws Exception {
                database.lowLevel().beginTransaction();
                try {
                    database.delete().byQuery(DeleteQuery.builder().table(MyReviewContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(myReviews).prepare().executeAsBlocking();
                    database.lowLevel().setTransactionSuccessful();
                } finally {
                    database.lowLevel().endTransaction();
                }
                e.onNext(myReviews);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<MyReviewModel>> loadMyReviews() {
        return Observable.create(new ObservableOnSubscribe<List<MyReviewModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<MyReviewModel>> e) throws Exception {
                List<MyReviewModel> myReviews = database.get().listOfObjects(MyReviewModel.class)
                        .withQuery(Query.builder().table(MyReviewContract.TABLE).build())
                        .prepare()
                        .executeAsBlocking();
                e.onNext(myReviews);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<PromoCodeModel>> savePromoCodes(List<PromoCodeModel> promoCodes) {
        return Observable.create(new ObservableOnSubscribe<List<PromoCodeModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<PromoCodeModel>> e) throws Exception {
                database.lowLevel().beginTransaction();
                try {
                    database.delete().byQuery(DeleteQuery.builder().table(PromoCodeContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(promoCodes).withPutResolver(new PromoCodePutResolver()).prepare().executeAsBlocking();
                    database.lowLevel().setTransactionSuccessful();
                } finally {
                    database.lowLevel().endTransaction();
                }
                e.onNext(promoCodes);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<PromoCodeModel>> loadPromoCodes() {
        return Observable.create(new ObservableOnSubscribe<List<PromoCodeModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<PromoCodeModel>> e) throws Exception {
                List<PromoCodeModel> promoCodes = database.get().listOfObjects(PromoCodeModel.class)
                        .withQuery(Query.builder().table(PromoCodeContract.TABLE).build())
                        .withGetResolver(new PromoCodeGetResolver())
                        .prepare()
                        .executeAsBlocking();
                e.onNext(promoCodes);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<FaqModel>> saveFaq(List<FaqModel> faqModels) {
        return Observable.create(new ObservableOnSubscribe<List<FaqModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<FaqModel>> e) throws Exception {
                database.lowLevel().beginTransaction();
                try {
                    database.delete().byQuery(DeleteQuery.builder().table(FaqContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(faqModels).prepare().executeAsBlocking();
                    database.lowLevel().setTransactionSuccessful();
                } finally {
                    database.lowLevel().endTransaction();
                }
                e.onNext(faqModels);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<FaqModel>> loadFaq() {
        return Observable.create(new ObservableOnSubscribe<List<FaqModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<FaqModel>> e) throws Exception {
                List<FaqModel> promoCodes = database.get().listOfObjects(FaqModel.class)
                        .withQuery(Query.builder().table(FaqContract.TABLE).build())
                        .prepare()
                        .executeAsBlocking();
                e.onNext(promoCodes);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<RuleModel>> saveRules(List<RuleModel> ruleModels) {
        return Observable.create(new ObservableOnSubscribe<List<RuleModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<RuleModel>> e) throws Exception {
                database.lowLevel().beginTransaction();
                try {
                    database.delete().byQuery(DeleteQuery.builder().table(RulesContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(ruleModels).prepare().executeAsBlocking();
                    database.lowLevel().setTransactionSuccessful();
                } finally {
                    database.lowLevel().endTransaction();
                }
                e.onNext(ruleModels);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<RuleModel>> loadRules() {
        return Observable.create(new ObservableOnSubscribe<List<RuleModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<RuleModel>> e) throws Exception {
                List<RuleModel> rules = database.get().listOfObjects(RuleModel.class)
                        .withQuery(Query.builder().table(RulesContract.TABLE).build())
                        .prepare()
                        .executeAsBlocking();
                e.onNext(rules);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<SliderModel>> saveSliders(List<SliderModel> sliders) {
        return Observable.create(new ObservableOnSubscribe<List<SliderModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<SliderModel>> e) throws Exception {
                database.lowLevel().beginTransaction();
                try {
                    database.delete().byQuery(DeleteQuery.builder().table(SliderContract.TABLE).build()).prepare().executeAsBlocking();
                    database.put().objects(sliders).withPutResolver(new SlidersPutResolver()).prepare().executeAsBlocking();
                    database.lowLevel().setTransactionSuccessful();
                } finally {
                    database.lowLevel().endTransaction();
                }
                e.onNext(sliders);
                e.onComplete();
            }
        });
    }

    @Override
    public Observable<List<SliderModel>> loadSliders() {
        return Observable.create(new ObservableOnSubscribe<List<SliderModel>>() {
            @Override
            public void subscribe(ObservableEmitter<List<SliderModel>> e) throws Exception {
                List<SliderModel> sliders = database.get().listOfObjects(SliderModel.class)
                        .withQuery(Query.builder().table(SliderContract.TABLE).build())
                        .withGetResolver(new SlidersGetResolver())
                        .prepare()
                        .executeAsBlocking();
                e.onNext(sliders);
                e.onComplete();
            }
        });
    }
}
