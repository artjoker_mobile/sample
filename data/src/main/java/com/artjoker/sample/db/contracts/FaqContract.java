package com.artjoker.sample.smartysale.db.contracts;

public interface FaqContract {
    String TABLE = "faq";

    interface FaqColumns {
        String ID = "id";

        String QUESTION = "question";

        String ANSWER = "answer";

        String POS = "pos";
    }
}