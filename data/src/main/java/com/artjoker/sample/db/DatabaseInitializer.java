package com.artjoker.sample.smartysale.db;

import android.content.Context;

import com.pushtorefresh.storio.sqlite.SQLiteTypeMapping;
import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.impl.DefaultStorIOSQLite;
import com.artjoker.sample.smartysale.model.FaqModel;
import com.artjoker.sample.smartysale.model.FaqModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.FaqModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.FaqModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.Order;
import com.artjoker.sample.smartysale.model.OrderStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.OrderStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.OrderStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.PromoCodeModel;
import com.artjoker.sample.smartysale.model.PromoCodeModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.PromoCodeModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.PromoCodeModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.RuleModel;
import com.artjoker.sample.smartysale.model.RuleModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.RuleModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.RuleModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.CategoryModel;
import com.artjoker.sample.smartysale.model.shop.CategoryModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.CategoryModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.CategoryModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.CouponModel;
import com.artjoker.sample.smartysale.model.shop.CouponModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.CouponModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.CouponModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.MyReviewModel;
import com.artjoker.sample.smartysale.model.shop.MyReviewModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.MyReviewModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.MyReviewModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.NewsModel;
import com.artjoker.sample.smartysale.model.shop.NewsModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.NewsModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.NewsModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.QueryModel;
import com.artjoker.sample.smartysale.model.shop.QueryModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.QueryModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.QueryModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.ReviewModel;
import com.artjoker.sample.smartysale.model.shop.ReviewModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.ReviewModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.ReviewModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.ShopModel;
import com.artjoker.sample.smartysale.model.shop.ShopModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.ShopModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.ShopModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.SliderModel;
import com.artjoker.sample.smartysale.model.shop.SliderModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.SliderModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.SliderModelStorIOSQLitePutResolver;
import com.artjoker.sample.smartysale.model.shop.TariffModel;
import com.artjoker.sample.smartysale.model.shop.TariffModelStorIOSQLiteDeleteResolver;
import com.artjoker.sample.smartysale.model.shop.TariffModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.TariffModelStorIOSQLitePutResolver;

public class DatabaseInitializer {

    private DbOpenHelper dbHelper;

    public StorIOSQLite initDatabase(Context context) {
        dbHelper = new DbOpenHelper(context);

        return DefaultStorIOSQLite.builder()
                .sqliteOpenHelper(dbHelper)
                .addTypeMapping(ShopModel.class, SQLiteTypeMapping.<ShopModel>builder()
                        .putResolver(new ShopModelStorIOSQLitePutResolver())
                        .getResolver(new ShopModelStorIOSQLiteGetResolver())
                        .deleteResolver(new ShopModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(CategoryModel.class, SQLiteTypeMapping.<CategoryModel>builder()
                        .putResolver(new CategoryModelStorIOSQLitePutResolver())
                        .getResolver(new CategoryModelStorIOSQLiteGetResolver())
                        .deleteResolver(new CategoryModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(QueryModel.class, SQLiteTypeMapping.<QueryModel>builder()
                        .putResolver(new QueryModelStorIOSQLitePutResolver())
                        .getResolver(new QueryModelStorIOSQLiteGetResolver())
                        .deleteResolver(new QueryModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(ShopDetailsModel.class, SQLiteTypeMapping.<ShopDetailsModel>builder()
                        .putResolver(new ShopDetailsModelStorIOSQLitePutResolver())
                        .getResolver(new ShopDetailsModelStorIOSQLiteGetResolver())
                        .deleteResolver(new ShopDetailsModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(CouponModel.class, SQLiteTypeMapping.<CouponModel>builder()
                        .putResolver(new CouponModelStorIOSQLitePutResolver())
                        .getResolver(new CouponModelStorIOSQLiteGetResolver())
                        .deleteResolver(new CouponModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(ReviewModel.class, SQLiteTypeMapping.<ReviewModel>builder()
                        .putResolver(new ReviewModelStorIOSQLitePutResolver())
                        .getResolver(new ReviewModelStorIOSQLiteGetResolver())
                        .deleteResolver(new ReviewModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(TariffModel.class, SQLiteTypeMapping.<TariffModel>builder()
                        .putResolver(new TariffModelStorIOSQLitePutResolver())
                        .getResolver(new TariffModelStorIOSQLiteGetResolver())
                        .deleteResolver(new TariffModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(NewsModel.class, SQLiteTypeMapping.<NewsModel>builder()
                        .putResolver(new NewsModelStorIOSQLitePutResolver())
                        .getResolver(new NewsModelStorIOSQLiteGetResolver())
                        .deleteResolver(new NewsModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(Order.class, SQLiteTypeMapping.<Order>builder()
                        .putResolver(new OrderStorIOSQLitePutResolver())
                        .getResolver(new OrderStorIOSQLiteGetResolver())
                        .deleteResolver(new OrderStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(MyReviewModel.class, SQLiteTypeMapping.<MyReviewModel>builder()
                        .putResolver(new MyReviewModelStorIOSQLitePutResolver())
                        .getResolver(new MyReviewModelStorIOSQLiteGetResolver())
                        .deleteResolver(new MyReviewModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(SliderModel.class, SQLiteTypeMapping.<SliderModel>builder()
                        .putResolver(new SliderModelStorIOSQLitePutResolver())
                        .getResolver(new SliderModelStorIOSQLiteGetResolver())
                        .deleteResolver(new SliderModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(PromoCodeModel.class, SQLiteTypeMapping.<PromoCodeModel>builder()
                        .putResolver(new PromoCodeModelStorIOSQLitePutResolver())
                        .getResolver(new PromoCodeModelStorIOSQLiteGetResolver())
                        .deleteResolver(new PromoCodeModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(FaqModel.class, SQLiteTypeMapping.<FaqModel>builder()
                        .putResolver(new FaqModelStorIOSQLitePutResolver())
                        .getResolver(new FaqModelStorIOSQLiteGetResolver())
                        .deleteResolver(new FaqModelStorIOSQLiteDeleteResolver())
                        .build())
                .addTypeMapping(RuleModel.class, SQLiteTypeMapping.<RuleModel>builder()
                        .putResolver(new RuleModelStorIOSQLitePutResolver())
                        .getResolver(new RuleModelStorIOSQLiteGetResolver())
                        .deleteResolver(new RuleModelStorIOSQLiteDeleteResolver())
                        .build())
                .build();
    }

    public DbOpenHelper getDbHelper() {
        return dbHelper;
    }
}
