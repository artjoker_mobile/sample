package com.artjoker.sample.smartysale.db.tables;

import android.support.annotation.NonNull;

import com.artjoker.sample.smartysale.db.contracts.CategoryContract;
import com.artjoker.sample.smartysale.db.tables.utils.TableTypes;
import com.artjoker.sample.smartysale.db.tables.utils.TableUtils;

public class CategoryTable {

    @NonNull
    public static String create() {
        return TableUtils.createTable(CategoryContract.TABLE,
                CategoryContract.CategoryColumns.ID + " " + TableTypes.INTEGER + ", "
                + CategoryContract.CategoryColumns.TITLE + " " + TableTypes.TEXT + ", "
                + CategoryContract.CategoryColumns.SHOPS_COUNT + " " + TableTypes.INTEGER);
    }

    @NonNull
    public static String drop() {
        return TableUtils.dropTable(CategoryContract.TABLE);
    }

}
