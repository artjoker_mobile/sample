package com.artjoker.sample.smartysale.db.tables;

import android.support.annotation.NonNull;

import com.artjoker.sample.smartysale.db.contracts.RulesContract;
import com.artjoker.sample.smartysale.db.tables.utils.TableTypes;
import com.artjoker.sample.smartysale.db.tables.utils.TableUtils;

public class RulesTable {

    @NonNull
    public static String create() {
        return TableUtils.createTable(RulesContract.TABLE, RulesContract.RulesColumns.NUMBER + " " + TableTypes.INTEGER + ", "
                + RulesContract.RulesColumns.TITLE + " " + TableTypes.TEXT + ", "
                + RulesContract.RulesColumns.PARAGRAPHS + " " + TableTypes.TEXT);
    }

    @NonNull
    public static String drop() {
        return TableUtils.dropTable(RulesContract.TABLE);
    }

}
