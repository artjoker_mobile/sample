package com.artjoker.sample.smartysale.db.resolvers;

import android.database.Cursor;
import android.support.annotation.NonNull;

import com.pushtorefresh.storio.sqlite.StorIOSQLite;
import com.pushtorefresh.storio.sqlite.queries.Query;
import com.artjoker.sample.smartysale.db.contracts.CouponContract;
import com.artjoker.sample.smartysale.db.contracts.ReviewContract;
import com.artjoker.sample.smartysale.db.contracts.TariffContract;
import com.artjoker.sample.smartysale.model.shop.CouponModel;
import com.artjoker.sample.smartysale.model.shop.CouponModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.ReviewModel;
import com.artjoker.sample.smartysale.model.shop.ReviewModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModelStorIOSQLiteGetResolver;
import com.artjoker.sample.smartysale.model.shop.TariffModel;
import com.artjoker.sample.smartysale.model.shop.TariffModelStorIOSQLiteGetResolver;

import java.util.ArrayList;

public class ShopDetailsGetResolver extends ShopDetailsModelStorIOSQLiteGetResolver {

    private StorIOSQLite storIOSQLite;
    private CouponModelStorIOSQLiteGetResolver couponModelStorIOSQLiteGetResolver;
    private ReviewModelStorIOSQLiteGetResolver reviewModelStorIOSQLiteGetResolver;
    private TariffModelStorIOSQLiteGetResolver tariffModelStorIOSQLiteGetResolver;

    public ShopDetailsGetResolver(StorIOSQLite storIOSQLite) {
        this.storIOSQLite = storIOSQLite;
        couponModelStorIOSQLiteGetResolver = new CouponModelStorIOSQLiteGetResolver();
        reviewModelStorIOSQLiteGetResolver = new ReviewModelStorIOSQLiteGetResolver();
        tariffModelStorIOSQLiteGetResolver = new TariffModelStorIOSQLiteGetResolver();
    }

    @NonNull
    @Override
    public ShopDetailsModel mapFromCursor(@NonNull Cursor cursor) {
        ShopDetailsModel shopDetails = super.mapFromCursor(cursor);
        shopDetails.setCoupons(new ArrayList<>());
        shopDetails.setReviews(new ArrayList<>());
        shopDetails.setTariffs(new ArrayList<>());

        getCoupons(shopDetails);
        getReviews(shopDetails);
        getTariffs(shopDetails);

        return shopDetails;
    }

    private void getCoupons(ShopDetailsModel shopDetails) {
        Cursor couponsCursor = storIOSQLite.lowLevel().query(Query.builder()
                .table(CouponContract.TABLE)
                .where(CouponContract.CouponColumns.SHOP_ID + " = ?")
                .whereArgs(shopDetails.getId())
                .build());

        couponsCursor.moveToFirst();
        while(!couponsCursor.isAfterLast()) {
            CouponModel couponModel = couponModelStorIOSQLiteGetResolver.mapFromCursor(couponsCursor);
            shopDetails.getCoupons().add(couponModel);
            couponsCursor.moveToNext();
        }
    }

    private void getReviews(ShopDetailsModel shopDetails) {
        Cursor reviewsCursor = storIOSQLite.lowLevel().query(Query.builder()
                .table(ReviewContract.TABLE)
                .where(ReviewContract.ReviewColumns.SHOP_ID + " = ?")
                .whereArgs(shopDetails.getId())
                .build());

        reviewsCursor.moveToFirst();
        while(!reviewsCursor.isAfterLast()) {
            ReviewModel reviewModel = reviewModelStorIOSQLiteGetResolver.mapFromCursor(reviewsCursor);
            shopDetails.getReviews().add(reviewModel);
            reviewsCursor.moveToNext();
        }
    }

    private void getTariffs(ShopDetailsModel shopDetails) {
        Cursor tariffsCursor = storIOSQLite.lowLevel().query(Query.builder()
                .table(TariffContract.TABLE)
                .where(TariffContract.TariffColumns.SHOP_ID + " = ?")
                .whereArgs(shopDetails.getId())
                .build());

        tariffsCursor.moveToFirst();
        while(!tariffsCursor.isAfterLast()) {
            TariffModel tariffModel = tariffModelStorIOSQLiteGetResolver.mapFromCursor(tariffsCursor);
            shopDetails.getTariffs().add(tariffModel);
            tariffsCursor.moveToNext();
        }
    }
}
