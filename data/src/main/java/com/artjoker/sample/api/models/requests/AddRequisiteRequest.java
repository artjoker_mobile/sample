package com.artjoker.sample.smartysale.api.models.requests;

import com.google.gson.annotations.Expose;

/**
 * Created by android on 03.05.2017.
 */

public class AddRequisiteRequest {

    @Expose
    private String currency;
    @Expose
    private String method;
    @Expose
    private String requisites;
    @Expose
    private String fio;
    @Expose
    private String phone;

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getRequisites() {
        return requisites;
    }

    public void setRequisites(String requisites) {
        this.requisites = requisites;
    }

    public String getFio() {
        return fio;
    }

    public void setFio(String fio) {
        this.fio = fio;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
}
