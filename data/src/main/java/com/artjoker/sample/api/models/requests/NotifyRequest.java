package com.artjoker.sample.smartysale.api.models.requests;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotifyRequest {

    public static final String NOTIFY = "notify";

    @Expose
    @SerializedName(NOTIFY)
    private Notify notify;

    public NotifyRequest(String name, int value) {
        this.notify = new Notify(name, value);
    }

    public Notify getNotify() {
        return notify;
    }

    class Notify {
        @Expose
        private String name;
        @Expose
        private int value;

        public Notify(String name, int value) {
            this.name = name;
            this.value = value;
        }

        public String getName() {
            return name;
        }

        public int getValue() {
            return value;
        }
    }
}
