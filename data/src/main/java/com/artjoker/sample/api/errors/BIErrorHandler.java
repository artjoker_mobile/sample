package com.artjoker.sample.smartysale.api.errors;

import java.util.Map;

public interface BIErrorHandler {

    void processNetError(Throwable throwable);

    void showError(Map<String, String> errors);
}
