package com.artjoker.sample.smartysale.api;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.artjoker.sample.smartysale.preferences.PrefStorage;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.Cache;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import smarty.artjoker.data.BuildConfig;

public final class Communicator {
    private static AppServerSpecs mAppInstance = null;

    @Inject
    PrefStorage prefStorage;

    @Inject
    public Communicator() {
    }


    public AppServerSpecs getAppServer() {
        if (mAppInstance == null) {
            OkHttpClient client = initOkHttpClient();
            Gson converter = new GsonBuilder()
                    .excludeFieldsWithoutExposeAnnotation()
                    .create();
            String baseUrl = BuildConfig.HOST;
            Retrofit retrofit = getRetrofit(client, converter, baseUrl);
            mAppInstance = retrofit.create(AppServerSpecs.class);
        }

        return mAppInstance;
    }

    @NonNull
    private OkHttpClient initOkHttpClient() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient.Builder builder = getHttpBuilder(interceptor);

        return builder.build();
    }

    @NonNull
    private static Retrofit getRetrofit(OkHttpClient client, Gson converter, String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(converter))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build();
    }

    @NonNull
    private static OkHttpClient.Builder getHttpBuilder(HttpLoggingInterceptor interceptor) {
        return new OkHttpClient.Builder()
                .readTimeout(30, TimeUnit.SECONDS)
                .connectTimeout(30, TimeUnit.SECONDS)
                .writeTimeout(30, TimeUnit.SECONDS)
                .retryOnConnectionFailure(true)
                .addInterceptor(interceptor);
    }

}