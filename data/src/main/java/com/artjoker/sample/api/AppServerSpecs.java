package com.artjoker.sample.smartysale.api;


import com.artjoker.sample.smartysale.api.models.requests.AddRequisiteRequest;
import com.artjoker.sample.smartysale.api.models.requests.ChangeLoginParams;
import com.artjoker.sample.smartysale.api.models.requests.ConfirmParams;
import com.artjoker.sample.smartysale.api.models.requests.NotifyRequest;
import com.artjoker.sample.smartysale.api.models.requests.ReferrerSignUpParams;
import com.artjoker.sample.smartysale.api.models.requests.SignUpParams;
import com.artjoker.sample.smartysale.api.models.responses.AddReviewResponse;
import com.artjoker.sample.smartysale.api.models.responses.ApplyPromoCodeResponse;
import com.artjoker.sample.smartysale.api.models.responses.AuthorizationModel;
import com.artjoker.sample.smartysale.api.models.responses.ChangeLoginResponse;
import com.artjoker.sample.smartysale.api.models.responses.ChangePasswordModel;
import com.artjoker.sample.smartysale.api.models.responses.ConfirmModel;
import com.artjoker.sample.smartysale.api.models.responses.ConfirmRequisiteResponse;
import com.artjoker.sample.smartysale.api.models.responses.Contacts;
import com.artjoker.sample.smartysale.api.models.responses.DetectCountryModel;
import com.artjoker.sample.smartysale.api.models.responses.PayoutMethod;
import com.artjoker.sample.smartysale.api.models.responses.Response;
import com.artjoker.sample.smartysale.api.models.responses.RestorePasswordModel;
import com.artjoker.sample.smartysale.api.models.responses.ShopsResponse;
import com.artjoker.sample.smartysale.api.models.responses.SignUpModel;
import com.artjoker.sample.smartysale.api.models.responses.UpdateProfileResponse;
import com.artjoker.sample.smartysale.model.FaqModel;
import com.artjoker.sample.smartysale.model.Notify;
import com.artjoker.sample.smartysale.model.Order;
import com.artjoker.sample.smartysale.model.PayoutModel;
import com.artjoker.sample.smartysale.model.PromoCodeModel;
import com.artjoker.sample.smartysale.model.ReferralModel;
import com.artjoker.sample.smartysale.model.Requisite;
import com.artjoker.sample.smartysale.model.RuleModel;
import com.artjoker.sample.smartysale.model.country.CountryModel;
import com.artjoker.sample.smartysale.model.profile.MinimalProfileModel;
import com.artjoker.sample.smartysale.model.profile.UpdateProfileModel;
import com.artjoker.sample.smartysale.model.shop.CouponModel;
import com.artjoker.sample.smartysale.model.shop.MyReviewModel;
import com.artjoker.sample.smartysale.model.shop.NewsModel;
import com.artjoker.sample.smartysale.model.shop.NewsViewModel;
import com.artjoker.sample.smartysale.model.shop.ReviewModel;
import com.artjoker.sample.smartysale.model.shop.ShopDetailsModel;
import com.artjoker.sample.smartysale.model.shop.SliderModel;
import com.artjoker.sample.smartysale.model.user.Currency;
import com.artjoker.sample.smartysale.model.user.User;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Observable;
import okhttp3.RequestBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.HTTP;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AppServerSpecs {
    String PREFIX = "api/app/v1";

    String CABINET = "/cabinet";
    String DEFAULT = "/default";
    String USER = "/user";



    @POST(PREFIX + USER + CONFIRM)
    Observable<ConfirmModel> confirmation(@Body ConfirmParams params);

    @GET(PREFIX + DEFAULT + COUNTIES)
    Observable<List<CountryModel>> getCountries();

    @GET(PREFIX + DEFAULT + DETECT_COUNTRY)
    Observable<DetectCountryModel> detectCountry();

    @GET(PREFIX + DEFAULT + CONTACTS)
    Observable<Contacts> getContacts();

    @GET(PREFIX + CABINET + PROFILE)
    Observable<User> getProfile();

    @DELETE(PREFIX + CABINET + AVATAR)
    Observable<Response> deleteAvatar();

    @GET(PREFIX + CABINET + NOTIFY)
    Observable<Notify> getNotify();

    @POST(PREFIX + CABINET + NOTIFY)
    Completable changeNotify(@Body NotifyRequest notifyRequest);

    @GET(PREFIX + CABINET + ORDERS)
    Observable<List<Order>> getOrders();


    @GET(PREFIX + CABINET + REVIEWS)
    Observable<List<MyReviewModel>> getMyReviews();

    @GET(PREFIX + DEFAULT + HELP)
    Observable<List<FaqModel>> getFaq();

    @GET(PREFIX + DEFAULT + RULES)
    Observable<List<RuleModel>> getRules();

    @GET(PREFIX + CABINET + REFERRALS)
    Observable<ArrayList<ReferralModel>> getReferrals();

