package com.artjoker.sample.smartysale.api.models.responses;

import com.google.gson.annotations.Expose;

public class ErrorModel {

    @Expose
    private String name;
    @Expose
    private String message;
    @Expose
    private String code;
    @Expose
    private String status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
