package com.artjoker.sample.smartysale.api.models.responses;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class Response {

    public static String PAGE_COUNT_HEADER = "X-Pagination-Page-Count";

    @Expose
    private long result;

    @Expose
    @SerializedName("error")
    private Map<String, String> error;

    public long getResult() {
        return result;
    }

    public void setResult(long result) {
        this.result = result;
    }

    public Map<String, String> getError() {
        return error;
    }

    public void setError(Map<String, String> error) {
        this.error = error;
    }
}