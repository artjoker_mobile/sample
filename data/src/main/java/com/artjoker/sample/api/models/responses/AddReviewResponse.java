package com.artjoker.sample.smartysale.api.models.responses;

import com.google.gson.annotations.Expose;
import com.artjoker.sample.smartysale.model.shop.ReviewModel;

import java.io.Serializable;

public class AddReviewResponse extends Response implements Serializable {

    @Expose
    private ReviewModel review;

    @Expose
    private boolean moder;

    public ReviewModel getReview() {
        return review;
    }

    public void setReview(ReviewModel review) {
        this.review = review;
    }

    public boolean isModer() {
        return moder;
    }

    public void setModer(boolean moder) {
        this.moder = moder;
    }
}
