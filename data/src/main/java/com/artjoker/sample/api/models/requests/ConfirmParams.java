package com.artjoker.sample.smartysale.api.models.requests;

import com.google.gson.annotations.Expose;

public class ConfirmParams {

    public static final String SIGN_UP_SCENARIO = "signup";
    public static final String RESTORE_PASSWORD_SCENARIO = "restore_pass";

    @Expose
    private String scenario;
    @Expose
    private String login;
    @Expose
    private String token;
    @Expose
    private long userId;
    @Expose
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getScenario() {
        return scenario;
    }

    public void setScenario(String scenario) {
        this.scenario = scenario;
    }
}
