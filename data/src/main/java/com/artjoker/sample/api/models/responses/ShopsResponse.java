package com.artjoker.sample.smartysale.api.models.responses;

import com.google.gson.annotations.Expose;
import com.artjoker.sample.smartysale.model.shop.CategoryModel;
import com.artjoker.sample.smartysale.model.shop.QueryModel;
import com.artjoker.sample.smartysale.model.shop.ShopModel;

import java.util.List;

public class ShopsResponse {
    @Expose
    private List<QueryModel> queries;
    @Expose
    private List<CategoryModel> categories;
    @Expose
    private List<ShopModel> shops;

    public List<QueryModel> getQueries() {
        return queries;
    }

    public void setQueries(List<QueryModel> queries) {
        this.queries = queries;
    }

    public List<CategoryModel> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryModel> categories) {
        this.categories = categories;
    }

    public List<ShopModel> getShops() {
        return shops;
    }

    public void setShops(List<ShopModel> shops) {
        this.shops = shops;
    }
}
